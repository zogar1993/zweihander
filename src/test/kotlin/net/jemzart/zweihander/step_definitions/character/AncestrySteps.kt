package net.jemzart.zweihander.step_definitions.character

import cucumber.api.java.en.Given
import net.jemzart.zweihander.state.setAncestry

class AncestrySteps {
	@Given("{string} has an ancestry of {string}")
	fun `{character} has an ancestry of {ancestry}`(character: String, ancestry: String) {
		setAncestry(character, ancestry)
	}
}