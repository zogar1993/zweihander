package net.jemzart.zweihander.step_definitions.character

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import net.jemzart.zweihander.core.domain.character.attribute.Attribute
import net.jemzart.zweihander.state.getCharacterSheet
import net.jemzart.zweihander.state.setAttributeAdvances
import net.jemzart.zweihander.state.setAttributeBase
import org.junit.Assert

class AttributeSteps {

	@Given("{string} has a {string} base chance of {int}")
	fun `{character} has a {attribute} base chance of {amount}`(
		character: String, attribute: String, amount: Int) {
		setAttributeBase(character, attribute, amount)
	}

	@Given("{string} has {int} advances in attribute {string}")
	fun `{name} has {amount} advances in attribute {attribute}`(
		character: String, amount: Int, attribute: String) {
		setAttributeAdvances(character, attribute, amount)
	}

	@Then("{string} must have a {string} base chance of {int}")
	fun `{character} must have a {attribute} base chance of {amount}`(
		name: String, attribute: String, amount: Int) {
		val character = getCharacterSheet(name)
		val base = character[Attribute.from(attribute)].base
		Assert.assertEquals(amount, base)
	}

	@Then("{string} must have a {string} bonus of {int}")
	fun `{character} must have {attribute} bonus of {amount}`(
		name: String, attribute: String, amount: Int) {
		val character = getCharacterSheet(name)
		val bonus = character[Attribute.from(attribute)].bonus
		Assert.assertEquals(amount, bonus)
	}
}