package net.jemzart.zweihander.step_definitions.character

import cucumber.api.java.en.Then
import net.jemzart.zweihander.state.getCharacterSheet
import org.junit.Assert

class CalculatedValuesSteps {
	@Then("{string} must have a damage threshold value of {int}")
	fun `{character} must have a damage threshold value of {value}`(characterName: String, value: Int) {
		val character = getCharacterSheet(characterName)
		Assert.assertEquals(character.damageThreshold, value)
	}
}