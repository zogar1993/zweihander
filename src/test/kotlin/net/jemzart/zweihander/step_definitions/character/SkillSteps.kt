package net.jemzart.zweihander.step_definitions.character

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import net.jemzart.zweihander.core.domain.character.skill.Skill
import net.jemzart.zweihander.state.*
import org.junit.Assert

class SkillSteps {
	@Given("{string} has {int} ranks in {string}")
	fun `{character} has {amount} ranks in {skill}`(
		character: String, amount: Int, skill: String) {
		setSkillRanks(character, skill, amount)
	}

	@When("{string} performs a {string} skill test")
	fun `{character} performs a {skill} skill test`(character: String, skill: String) {
		skillTest(character, skill)
	}

	@When("{string} performs a {string} skill test and rolls {int}")
	fun `{character} performs a {skill} skill test and rolls {number}`(
		character: String, skill: String, number: Int) {
		skillTest(character, skill)
		setD100Roll(number)
	}

	@When("{string} performs a {string} skill test and rolls a {string}")
	fun `{character} performs a {skill} skill test and rolls a {result}`(
		character: String, skill: String, resultType: String) {
		val number = resultTypeToNumber(resultType)
		`{character} performs a {skill} skill test and rolls {number}`(character, skill, number)
	}

	@When("{string} performs a {string} {string} skill test")
	fun `{character} performs a {difficulty} {skill} skill test`(
		character: String, difficulty: String, skill: String) {
		skillTestWithDifficulty(character, skill, difficulty)
	}

	@Then("{string} must have {string} skill chance of {int}")
	fun `{character} must have {attribute} skill chance of {amount}`(
		name: String, skill: String, amount: Int) {
		val character = getCharacterSheet(name)
		val chance = character[Skill.from(skill)].chance
		Assert.assertEquals(amount, chance)
	}
}