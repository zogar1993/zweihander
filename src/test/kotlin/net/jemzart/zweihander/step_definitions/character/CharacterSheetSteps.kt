package net.jemzart.zweihander.step_definitions.character

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import net.jemzart.zweihander.core.domain.character.DamageConditionState
import net.jemzart.zweihander.state.*
import org.junit.Assert

class CharacterSheetSteps {
	@Given("{string} is a character sheet")
	fun `{character} is a character sheet`(name: String) {
		createCharacterSheet(name)
	}

	@Given("{string} has {int} action points left")
	fun `{combatant} has {amount} action points left`(name: String, amount: Int) {
		setCombatantActionPoints(name, amount)
	}

	@Given("{string} is {string} in the damage condition track")
	fun `{character} is {state} in the damage condition track`(name: String, state: String) {

	}

	@Given("{string} engages {string}")
	fun `{engager} engages {engaged}`(engager: String, engaged: String) {
		engageWith(engager, engaged)
	}

	@Given("{string} has a {string} in her main hand")
	fun `{character} has a {weapon} in her main hand`(character: String, weapon: String) {

	}

	@Then("{string} must have {int} action points left")
	fun `{character} has {amount} action points left validation`(name: String, amount: Int) {
		val combatant = getCombatant(name, TEST_SESSION_NAME)
		Assert.assertEquals(amount, combatant.ap)
	}

	@Then("{string} must be {string} in the damage condition track")
	fun `{character} is {state} in the damage condition track validation`(name: String, stateName: String) {
		val combatant = getCombatant(name, TEST_SESSION_NAME)
		val state = DamageConditionState.from(stateName)
		Assert.assertEquals(state, combatant.damageCondition)
	}
}