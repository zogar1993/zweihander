package net.jemzart.zweihander.step_definitions.character

import cucumber.api.java.en.Given
import net.jemzart.zweihander.state.setProfession

class ProfessionSteps {
	@Given("{string} has a basic tier profession of {string}")
	fun `{character} has a basic tier profession of {profession}`(
		character: String, profession: String) {
		setProfession(character, profession)
	}
}