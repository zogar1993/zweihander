package net.jemzart.zweihander.step_definitions.misc

import cucumber.api.java.en.Given
import net.jemzart.zweihander.state.TEST_SESSION_NAME
import net.jemzart.zweihander.state.executeGameSessionCommand

class ChoicesSteps {
	@Given("{string} chooses {string}")
	fun `{character} chooses {skill}`(character: String, skill: String) {
		executeGameSessionCommand("choose_skill $skill", TEST_SESSION_NAME)
	}
}