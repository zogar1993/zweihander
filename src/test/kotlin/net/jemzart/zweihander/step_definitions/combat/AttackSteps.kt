package net.jemzart.zweihander.step_definitions.combat

import cucumber.api.java.en.When
import net.jemzart.zweihander.state.meleeAttack
import net.jemzart.zweihander.state.resultTypeToNumber
import net.jemzart.zweihander.state.setD100Roll

class AttackSteps {
	@When("{string} attacks {string} with {string} and rolls {int}")
	fun `{attacker} attacks {victim} with {weapon} and rolls {number}`(
		attacker: String, victim: String, weapon: String, number: Int) {
		meleeAttack(attacker, victim, weapon)
		setD100Roll(number)
	}

	@When("{string} attacks {string} with {string} and rolls a {string}")
	fun `{attacker} attacks {victim} with {weapon} and rolls a {result}`(
		attacker: String, victim: String, weapon: String, result: String) {
		val number = resultTypeToNumber(result)
		`{attacker} attacks {victim} with {weapon} and rolls {number}`(attacker, victim, weapon, number)
	}

	@When("{string} attacks {string} with {string}")
	fun `{attacker} attacks {victim} with {weapon}`(
		attacker: String, victim: String, weapon: String) {
		meleeAttack(attacker, victim, weapon)
	}
}