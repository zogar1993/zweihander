package net.jemzart.zweihander.step_definitions.combat

import cucumber.api.java.en.When
import net.jemzart.zweihander.state.TEST_SESSION_NAME
import net.jemzart.zweihander.state.executeGameSessionCommand

class SpecialActionSteps {
	@When("{string} takes aim with 1 action point")
	fun `{character} takes aim with 1 action point`(character: String) {
		executeGameSessionCommand("take_aim $character 1", TEST_SESSION_NAME)
	}

	@When("{string} takes aim with 2 action points")
	fun `{character} takes aim with 2 action point`(character: String) {
		executeGameSessionCommand("take_aim $character 2", TEST_SESSION_NAME)
	}
}