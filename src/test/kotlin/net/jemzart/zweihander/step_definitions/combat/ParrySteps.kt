package net.jemzart.zweihander.step_definitions.combat

import cucumber.api.java.en.When
import net.jemzart.zweihander.state.TEST_SESSION_NAME
import net.jemzart.zweihander.state.executeGameSessionCommand
import net.jemzart.zweihander.state.resultTypeToNumber
import net.jemzart.zweihander.state.setD100Roll

class ParrySteps {
	@When("{string} parries with {string}")
	fun `{defender} parries with {weapon}`(
		defender: String, weapon: String) {
		executeGameSessionCommand("parry $defender $weapon", TEST_SESSION_NAME)
	}

	@When("{string} parries with {string} and rolls {int}")
	fun `{defender} parries with {weapon} and rolls {number}`(
		defender: String, weapon: String, number: Int) {
		`{defender} parries with {weapon}`(defender, weapon)
		setD100Roll(number)
	}

	@When("{string} parries with {string} and rolls a {string}")
	fun `{defender} parries with {weapon} and rolls a {result}`(
		defender: String, weapon: String, result: String) {
		val number = resultTypeToNumber(result)
		`{defender} parries with {weapon} and rolls {number}`(defender, weapon, number)
	}
}