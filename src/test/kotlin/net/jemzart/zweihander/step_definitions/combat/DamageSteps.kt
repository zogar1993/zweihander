package net.jemzart.zweihander.step_definitions.combat

import cucumber.api.java.en.When
import net.jemzart.zweihander.state.TEST_SESSION_NAME
import net.jemzart.zweihander.state.executeGameSessionCommand
import net.jemzart.zweihander.state.getSessionLog
import org.junit.Assert

class DamageSteps {
	@When("{string} rolls a {int} in her damage roll")
	fun `{combatant} rolls a {amount} in her damage roll`(name: String, amount: Int) {
		executeGameSessionCommand("roll_1d6 $amount", TEST_SESSION_NAME)
	}

	@When("{string} has received {int} damage")
	fun `{character} has received {amount} damage`(character: String, amount: Int) {
		val log = getSessionLog(TEST_SESSION_NAME)
		Assert.assertTrue("{character:$character} received $amount damage" in log)
	}
}