package net.jemzart.zweihander.step_definitions.combat

import cucumber.api.java.en.Given
import net.jemzart.zweihander.state.*

class CombatSteps {
	@Given("{string} initiates violence against {string}")
	fun `{attacker} initiates violence against {victim}`(attacker: String, victim: String) {
		meleeAttack(attacker, victim, "bare-handed")
		setD100Roll(98)
		confirmRoll()
	}

	@Given("{string} and {string} are allies")
	fun `{ally} and {ally} are allies`(allyA: String, allyB: String) {
		establishAllianceBetween(allyA, allyB)
	}

	@Given("combat breaks between {string} and {string}")
	fun `combat breaks between {characterA} and {characterB}`(characterA: String, characterB: String) {
		executeGameSessionCommand("start_combat", TEST_SESSION_NAME)
		`{character} joins the combat`(characterA)
		`{character} joins the combat`(characterB)
	}

	@Given("close combat breaks between {string} and {string}")
	fun `close combat breaks between {characterA} and {characterB}`(characterA: String, characterB: String) {
		`combat breaks between {characterA} and {characterB}`(characterA, characterB)
		engageWith(characterA, characterB)
	}

	@Given("{string} joins the combat")
	fun `{character} joins the combat`(character: String) {
		addCharacterToCombat(character)
	}

	@Given("{string} is a new character that joins the combat")
	fun `{character} is a new character that joins the combat`(character: String) {
		createCharacterSheet(character)
		addCharacterToSession(character, TEST_SESSION_NAME)
		addCharacterToCombat(character)
	}

	@Given("combat is over")
	fun `combat is over}`() {
		executeGameSessionCommand("end_scene", TEST_SESSION_NAME)
	}
}