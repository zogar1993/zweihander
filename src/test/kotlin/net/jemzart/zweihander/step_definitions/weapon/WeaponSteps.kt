package net.jemzart.zweihander.step_definitions.weapon

import cucumber.api.java.en.Given
import net.jemzart.zweihander.core.domain.Weapon
import net.jemzart.zweihander.core.domain.character.skill.Skill
import net.jemzart.zweihander.state.weapons

class WeaponSteps {
	@Given("{string} is a {string} weapon")
	fun `{weapon} is a {attribute} weapon`(weapon: String, attribute: String) {
		weapons[weapon] = Weapon(Skill.from(attribute))
	}
}