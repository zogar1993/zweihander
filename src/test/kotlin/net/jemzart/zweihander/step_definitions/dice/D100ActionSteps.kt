package net.jemzart.zweihander.step_definitions.dice

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import net.jemzart.zweihander.state.*
import org.junit.Assert

class D100ActionSteps {
	@Given("{string} rerolls the d100 with his trait {string} and rolls {int}")
	fun `{string} rerolls the d100 with his trait {string} and rolls {int}`(character: String, trait: String, number: Int) {
		executeGameSessionCommand("reroll_special $trait $character", TEST_SESSION_NAME)
		setD100Roll(number)
	}

	@Given("{string} rerolls the d100 with his trait {string} and rolls a {string}")
	fun `{character} rerolls the d100 with his trait {string} and rolls {int} a {result}`(character: String, trait: String, result: String) {
		val number = resultTypeToNumber(result)
		`{string} rerolls the d100 with his trait {string} and rolls {int}`(character, trait, number)
	}

	@Given("the d100 roll is confirmed")
	fun `the d100 roll is confirmed`() {
		confirmRoll()
	}

	@When("the d100 rolls {int}")
	fun `the d100 rolls {number}`(number: Int) {
		setD100Roll(number)
	}

	@Then("possible actions {string} include {string}")
	fun `possible actions {obligatoriness} include {action}`(
		obligatoriness: String, action: String) {
		val options = retrieveCurrentOptions(TEST_SESSION_NAME)
		when (obligatoriness) {
			"MUST" -> Assert.assertTrue(options.any { Regex(it).matches(action) })
			"MUST NOT" -> Assert.assertTrue(options.none { Regex(it).matches(action) })
			else -> Assert.fail()
		}
	}

	@Then("the roll is a {string}")
	fun `the roll is a {result type}`(resultType: String) {
		val rollInfo = getLastD100RollInfo(TEST_SESSION_NAME)
		Assert.assertEquals(resultType, rollInfo.resultType)
	}

	@Then("action total chance of success is {int}")
	fun `action total chance of success is {amount}`(amount: Int) {
		val rollInfo = getLastD100RollInfo(TEST_SESSION_NAME)
		Assert.assertEquals(amount, rollInfo.totalChance)
	}
}