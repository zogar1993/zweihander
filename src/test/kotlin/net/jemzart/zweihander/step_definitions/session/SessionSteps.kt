package net.jemzart.zweihander.step_definitions.session

import cucumber.api.java.en.Given
import net.jemzart.zweihander.state.TEST_SESSION_NAME
import net.jemzart.zweihander.state.addCharacterToSession
import net.jemzart.zweihander.state.createSession
import net.jemzart.zweihander.state.setFortunePoints

class SessionSteps {
	@Given("the party has {int} fortune points left")
	fun `the party has {amount} fortune points left`(amount: Int) {
		setFortunePoints(amount)
	}

	@Given("{string} joins the gaming session")
	fun `{name} is added to the session`(name: String) {
		addCharacterToSession(name, TEST_SESSION_NAME)
	}

	@Given("The gaming session starts")
	fun `The gaming session starts`() {
		createSession(TEST_SESSION_NAME)
	}

	@Given("The gaming session starts with {string}")
	fun `The gaming session starts with {string}`(name: String) {
		`The gaming session starts`()
		`{name} is added to the session`(name)
	}

	@Given("The gaming session starts with {string} and {string}")
	fun `The gaming session starts with {string} and {string}`(nameA: String, nameB: String) {
		`The gaming session starts`()
		`{name} is added to the session`(nameA)
		`{name} is added to the session`(nameB)
	}
}