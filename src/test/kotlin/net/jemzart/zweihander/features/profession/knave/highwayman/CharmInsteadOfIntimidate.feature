Feature: Highwayman 'stand_and_deliver' lets him use charm instead of intimidate

  Background:
    Given "pipin" is a character sheet
    And "pipin" has a basic tier profession of "highwayman"
    And "pipin" has a "brawn" base chance of 47
    And "pipin" has a "fellowship" base chance of 48
    And The gaming session starts with "pipin"

  Scenario Outline: Having chosen <chosen_skill>
    Given "pipin" performs a "intimidate" skill test
    When "pipin" chooses <chosen_skill>
    Then action total chance of success is <result>
    Examples:
      | chosen_skill | result |
      | intimidate   | 47     |
      | charm        | 48     |