Feature: Cheapjack 'Grease the Wheel' applied Counterfeit

  Background:
    Given "waylon" is a character sheet
    And "waylon" has a basic tier profession of "cheapjack"
    And "waylon" has a "intelligence" base chance of 40
    And "waylon" has 1 ranks in "counterfeit"
    And The gaming session starts with "waylon"

  Scenario: Anarchist rerolls failed d100 on counterfeit test
    When "waylon" performs a "counterfeit" skill test and rolls 51
    And the roll is a "failure"
    And "waylon" rerolls the d100 with his trait "grease_the_wheels" and rolls 50
    Then the roll is a "success"

  Scenario Outline: Character <reroll_posibility> be able to reroll a counterfeit test which resulted in <result>
    When "waylon" performs a "counterfeit" skill test and rolls a <result>
    Then possible actions <reroll_posibility> include "reroll_special grease_the_wheels waylon"
    And possible actions "MUST" include "confirm"
    Examples:
      | result             | reroll_posibility |
      | "failure"          | "MUST"            |
      | "success"          | "MUST NOT"        |
      | "critical failure" | "MUST NOT"        |
      | "critical success" | "MUST NOT"        |

  Scenario: Reroll is not possible twice in the same action
    When "waylon" performs a "counterfeit" skill test and rolls 51
    And "waylon" rerolls the d100 with his trait "grease_the_wheels" and rolls 51
    Then possible actions "MUST NOT" include "reroll_special grease_the_wheels waylon"
    And possible actions "MUST" include "confirm"
