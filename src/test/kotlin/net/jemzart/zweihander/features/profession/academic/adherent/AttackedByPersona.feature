Feature: Adherent 'Turn the other cheek' applied to another character

  Background:
    Given "helga" is a character sheet
    And "helga" has a basic tier profession of "adherent"
    And "calendula" is a character sheet
    And "calendula" has a "combat" base chance of 40
    And "calendula" has 0 ranks in "simple_melee"
    And The gaming session starts with "helga" and "calendula"
    And close combat breaks between "helga" and "calendula"

  Scenario: Adherent is attacked by a player ancestry
    When "calendula" attacks "helga" with "bare-handed"
    Then action total chance of success is 20

  Scenario: Adherent attacks and is attacked by a player ancestry
    Given "helga" initiates violence against "calendula"
    When "calendula" attacks "helga" with "bare-handed"
    Then action total chance of success is 40

  Scenario: Adherent attacks and is attacked by a player ancestry allied with the attacked
    Given "ayasta" is a new character that joins the combat
    And "ayasta" and "calendula" are allies
    And "ayasta" engages "helga"
    And "helga" initiates violence against "ayasta"
    When "calendula" attacks "helga" with "bare-handed"
    Then action total chance of success is 40

  Scenario: Adherent attacks and is attacked by a player ancestry in another combat
    Given "helga" initiates violence against "calendula"
    And combat is over
    And close combat breaks between "helga" and "calendula"
    When "calendula" attacks "helga" with "bare-handed"
    Then action total chance of success is 20

  Scenario: Adherent attacks and is attacked by a player ancestry not allied with the attacked
    Given "khorana" is a new character that joins the combat
    And "khorana" engages "helga"
    And "helga" initiates violence against "khorana"
    When "calendula" attacks "helga" with "bare-handed"
    Then action total chance of success is 20