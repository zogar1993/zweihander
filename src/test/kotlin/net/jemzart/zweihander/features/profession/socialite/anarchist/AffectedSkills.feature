Feature: Anarchist 'Rabble-Rousing' only applies to Guile and Leadership

  Background:
    Given "waylon" is a character sheet
    And "waylon" has a basic tier profession of "anarchist"
    And The gaming session starts with "waylon"

  Scenario Outline: Character <reroll_posibility> be able to reroll a failed <skill>
    When "waylon" performs a <skill> skill test and rolls a "failure"
    Then possible actions <reroll_posibility> include "reroll_special rabble-rousing waylon"
    Examples:
      | skill            | reroll_posibility |
      | "martial_melee"  | "MUST NOT"        |
      | "martial_ranged" | "MUST NOT"        |
      | "simple_melee"   | "MUST NOT"        |
      | "simple_ranged"  | "MUST NOT"        |
      | "athletics"      | "MUST NOT"        |
      | "drive"          | "MUST NOT"        |
      | "intimidate"     | "MUST NOT"        |
      | "toughness"      | "MUST NOT"        |
      | "coordination"   | "MUST NOT"        |
      | "pilot"          | "MUST NOT"        |
      | "ride"           | "MUST NOT"        |
      | "skulduggery"    | "MUST NOT"        |
      | "stealth"        | "MUST NOT"        |
      | "awareness"      | "MUST NOT"        |
      | "eavesdrop"      | "MUST NOT"        |
      | "scrutinize"     | "MUST NOT"        |
      | "survival"       | "MUST NOT"        |
      | "alchemy"        | "MUST NOT"        |
      | "counterfeit"    | "MUST NOT"        |
      | "education"      | "MUST NOT"        |
      | "folklore"       | "MUST NOT"        |
      | "gamble"         | "MUST NOT"        |
      | "heal"           | "MUST NOT"        |
      | "navigation"     | "MUST NOT"        |
      | "warfare"        | "MUST NOT"        |
      | "incantation"    | "MUST NOT"        |
      | "interrogation"  | "MUST NOT"        |
      | "resolve"        | "MUST NOT"        |
      | "tradecraft"     | "MUST NOT"        |
      | "bargain"        | "MUST NOT"        |
      | "charm"          | "MUST NOT"        |
      | "disguise"       | "MUST NOT"        |
      | "guile"          | "MUST"            |
      | "handle_animal"  | "MUST NOT"        |
      | "leadership"     | "MUST"            |
      | "rumor"          | "MUST NOT"        |