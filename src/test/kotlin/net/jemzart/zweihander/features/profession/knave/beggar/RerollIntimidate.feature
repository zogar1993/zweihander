Feature: Beggar 'beggars_bowl' applied Intimidate

  Background:
    Given "waylon" is a character sheet
    And "waylon" has a basic tier profession of "beggar"
    And "waylon" has a "brawn" base chance of 40
    And "waylon" has 1 ranks in "intimidate"
    And The gaming session starts with "waylon"

  Scenario: Beggar rerolls failed d100 on intimidate test
    When "waylon" performs a "intimidate" skill test and rolls 51
    And the roll is a "failure"
    And "waylon" rerolls the d100 with his trait "beggars_bowl" and rolls 50
    Then the roll is a "success"

  Scenario Outline: Character <reroll_posibility> be able to reroll a intimidate test which resulted in <result>
    When "waylon" performs a "intimidate" skill test and rolls a <result>
    Then possible actions <reroll_posibility> include "reroll_special beggars_bowl waylon"
    And possible actions "MUST" include "confirm"
    Examples:
      | result             | reroll_posibility |
      | "failure"          | "MUST"            |
      | "success"          | "MUST NOT"        |
      | "critical failure" | "MUST NOT"        |
      | "critical success" | "MUST NOT"        |

  Scenario: Reroll is not possible twice in the same action
    When "waylon" performs a "intimidate" skill test and rolls 51
    And "waylon" rerolls the d100 with his trait "beggars_bowl" and rolls 51
    Then possible actions "MUST NOT" include "reroll_special beggars_bowl waylon"
    And possible actions "MUST" include "confirm"
