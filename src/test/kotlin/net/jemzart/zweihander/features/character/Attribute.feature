Feature: Attribute Calculations
  I want attribute bonuses to be correctly calculated

  Background:
    Given "ragoz" is a character sheet

  Scenario: All attributes are loaded separately
    Given "ragoz" has a "combat" base chance of 49
    And "ragoz" has a "brawn" base chance of 48
    And "ragoz" has a "agility" base chance of 47
    And "ragoz" has a "perception" base chance of 46
    And "ragoz" has a "intelligence" base chance of 45
    And "ragoz" has a "willpower" base chance of 44
    And "ragoz" has a "fellowship" base chance of 43
    Then "ragoz" must have a "combat" base chance of 49
    And "ragoz" must have a "brawn" base chance of 48
    And "ragoz" must have a "agility" base chance of 47
    And "ragoz" must have a "perception" base chance of 46
    And "ragoz" must have a "intelligence" base chance of 45
    And "ragoz" must have a "willpower" base chance of 44
    And "ragoz" must have a "fellowship" base chance of 43

  Scenario Outline: A character is created with a <attribute> base chance of <base> and <advances> advances has a bonus of <bonus>
    Given "ragoz" has a <attribute> base chance of <base>
    And "ragoz" has <advances> advances in attribute <attribute>
    Then "ragoz" must have a <attribute> bonus of <bonus>

    Examples:
      | attribute      | base | bonus | advances |
      | "intelligence" | 40   | 4     | 0        |
      | "intelligence" | 40   | 5     | 1        |
      | "intelligence" | 40   | 6     | 2        |
      | "intelligence" | 40   | 7     | 3        |
      | "intelligence" | 49   | 4     | 0        |
      | "intelligence" | 30   | 3     | 0        |
      | "intelligence" | 39   | 3     | 0        |
      | "intelligence" | 39   | 4     | 1        |
      | "intelligence" | 39   | 5     | 2        |
      | "intelligence" | 39   | 6     | 3        |