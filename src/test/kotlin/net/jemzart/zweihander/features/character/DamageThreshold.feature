Feature: Damage Threshold

  Background:
    Given "ragoz" is a character sheet

  Scenario Outline: Ragoz has a BB of <bonus>, so his damage threshold is <threshold>
    Given "ragoz" has a "brawn" base chance of <base>
    And "ragoz" must have a "brawn" bonus of <bonus>
    Then "ragoz" must have a damage threshold value of <threshold>

    Examples:
      | base | bonus | threshold |
      | 30   | 3     | 6         |
      | 40   | 4     | 7         |
      | 50   | 5     | 8         |
