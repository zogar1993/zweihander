Feature: Ancestry attribute bonus calculation
  I want attributes bonuses to be correctly calculated based on ancestry

  Background:
    Given "ragoz" is a character sheet

  Scenario Outline: Attributes are calculated correctly when changing ancestry
    Given "ragoz" has an ancestry of "halfling"
    And "ragoz" has an ancestry of <ancestry>
    And "ragoz" has a <attribute> base chance of 40
    Then "ragoz" must have a <attribute> bonus of <bonus>
    Examples:
      | ancestry | attribute      | bonus |
      | "ogre"   | "combat"       | 5     |
      | "ogre"   | "intelligence" | 4     |
      | "ogre"   | "perception"   | 3     |
      | "ogre"   | "agility"      | 2     |
      | "ogre"   | "fellowship"   | 4     |
      | "ogre"   | "willpower"    | 4     |
      | "ogre"   | "brawn"        | 6     |

  Scenario Outline: A <ancestry> character and has a bonus
    Given "ragoz" has an ancestry of <ancestry>
    And "ragoz" has a <attribute> base chance of 40
    Then "ragoz" must have a <attribute> bonus of <bonus>
    Examples:
      | ancestry   | attribute      | bonus |
      | "human"    | "combat"       | 5     |
      | "human"    | "intelligence" | 5     |
      | "human"    | "perception"   | 5     |
      | "human"    | "agility"      | 3     |
      | "human"    | "fellowship"   | 3     |
      | "human"    | "willpower"    | 3     |
      | "human"    | "brawn"        | 4     |
      | "elf"      | "combat"       | 4     |
      | "elf"      | "intelligence" | 5     |
      | "elf"      | "perception"   | 5     |
      | "elf"      | "agility"      | 5     |
      | "elf"      | "fellowship"   | 3     |
      | "elf"      | "willpower"    | 3     |
      | "elf"      | "brawn"        | 3     |
      | "dwarf"    | "combat"       | 5     |
      | "dwarf"    | "intelligence" | 4     |
      | "dwarf"    | "perception"   | 3     |
      | "dwarf"    | "agility"      | 3     |
      | "dwarf"    | "fellowship"   | 3     |
      | "dwarf"    | "willpower"    | 5     |
      | "dwarf"    | "brawn"        | 5     |
      | "gnome"    | "combat"       | 3     |
      | "gnome"    | "intelligence" | 5     |
      | "gnome"    | "perception"   | 4     |
      | "gnome"    | "agility"      | 5     |
      | "gnome"    | "fellowship"   | 3     |
      | "gnome"    | "willpower"    | 5     |
      | "gnome"    | "brawn"        | 3     |
      | "halfling" | "combat"       | 3     |
      | "halfling" | "intelligence" | 3     |
      | "halfling" | "perception"   | 5     |
      | "halfling" | "agility"      | 5     |
      | "halfling" | "fellowship"   | 5     |
      | "halfling" | "willpower"    | 4     |
      | "halfling" | "brawn"        | 3     |
      | "ogre"     | "combat"       | 5     |
      | "ogre"     | "intelligence" | 4     |
      | "ogre"     | "perception"   | 3     |
      | "ogre"     | "agility"      | 2     |
      | "ogre"     | "fellowship"   | 4     |
      | "ogre"     | "willpower"    | 4     |
      | "ogre"     | "brawn"        | 6     |