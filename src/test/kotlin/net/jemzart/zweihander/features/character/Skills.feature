Feature: Attribute Calculations
  I want skill bonuses to be correctly calculated

  Background:
    Given "ragoz" is a character sheet

  Scenario Outline: <attribute>(<base>)+skill advances(<ranks>)*10 = <skill>base chance(<result>)
    Given "ragoz" has a <attribute> base chance of <base>
    And "ragoz" has <ranks> ranks in <skill>
    Then "ragoz" must have <skill> skill chance of <result>

    Examples:
      | attribute      | base | skill            | ranks | result |
      | "combat"       | 40   | "martial_melee"  | 0     | 40     |
      | "combat"       | 40   | "martial_ranged" | 0     | 40     |
      | "combat"       | 40   | "simple_melee"   | 0     | 40     |
      | "combat"       | 40   | "simple_ranged"  | 0     | 40     |
      | "brawn"        | 40   | "athletics"      | 0     | 40     |
      | "brawn"        | 40   | "drive"          | 0     | 40     |
      | "brawn"        | 40   | "intimidate"     | 0     | 40     |
      | "brawn"        | 40   | "toughness"      | 0     | 40     |
      | "agility"      | 40   | "coordination"   | 0     | 40     |
      | "agility"      | 40   | "pilot"          | 0     | 40     |
      | "agility"      | 40   | "ride"           | 0     | 40     |
      | "agility"      | 40   | "skulduggery"    | 0     | 40     |
      | "agility"      | 40   | "stealth"        | 0     | 40     |
      | "perception"   | 40   | "awareness"      | 0     | 40     |
      | "perception"   | 40   | "eavesdrop"      | 0     | 40     |
      | "perception"   | 40   | "scrutinize"     | 0     | 40     |
      | "perception"   | 40   | "survival"       | 0     | 40     |
      | "intelligence" | 40   | "alchemy"        | 0     | 40     |
      | "intelligence" | 40   | "counterfeit"    | 0     | 40     |
      | "intelligence" | 40   | "education"      | 0     | 40     |
      | "intelligence" | 40   | "folklore"       | 0     | 40     |
      | "intelligence" | 40   | "gamble"         | 0     | 40     |
      | "intelligence" | 40   | "heal"           | 0     | 40     |
      | "intelligence" | 40   | "navigation"     | 0     | 40     |
      | "intelligence" | 40   | "warfare"        | 0     | 40     |
      | "willpower"    | 40   | "incantation"    | 0     | 40     |
      | "willpower"    | 40   | "interrogation"  | 0     | 40     |
      | "willpower"    | 40   | "resolve"        | 0     | 40     |
      | "willpower"    | 40   | "tradecraft"     | 0     | 40     |
      | "fellowship"   | 40   | "bargain"        | 0     | 40     |
      | "fellowship"   | 40   | "charm"          | 0     | 40     |
      | "fellowship"   | 40   | "disguise"       | 0     | 40     |
      | "fellowship"   | 40   | "guile"          | 0     | 40     |
      | "fellowship"   | 40   | "handle_animal"  | 0     | 40     |
      | "fellowship"   | 40   | "leadership"     | 0     | 40     |
      | "fellowship"   | 40   | "rumor"          | 0     | 40     |
      | "fellowship"   | 40   | "rumor"          | 1     | 50     |
      | "fellowship"   | 40   | "rumor"          | 2     | 60     |
      | "fellowship"   | 40   | "rumor"          | 3     | 70     |
      | "fellowship"   | 55   | "rumor"          | 3     | 85     |
