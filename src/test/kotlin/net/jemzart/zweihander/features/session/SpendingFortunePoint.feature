Feature: Fortune Points
  Fortune points should only be able to be spend when you fail an action

  Background:
    And "carrie" is a character sheet
    And "carrie" has a "combat" base chance of 40
    And "carrie" has 0 ranks in "simple_melee"
    And "linuar" is a character sheet
    And The gaming session starts with "carrie" and "linuar"
    And close combat breaks between "carrie" and "linuar"
    And "carrie" has 1 action points left

  Scenario: A character attacks another character but misses
    Given the party has 1 fortune points left
    When "carrie" attacks "linuar" with "bare-handed" and rolls a "failure"
    Then possible actions "MUST" include "reroll_special fortune_point_d100_reroll carrie"
    And possible actions "MUST" include "confirm"

  Scenario: A character attacks another character but misses
    Given the party has 0 fortune points left
    When "carrie" attacks "linuar" with "bare-handed" and rolls a "failure"
    Then possible actions "MUST NOT" include "reroll_special fortune_point_d100_reroll carrie"
    And possible actions "MUST" include "confirm"