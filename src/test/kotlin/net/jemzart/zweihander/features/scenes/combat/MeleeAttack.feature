Feature: Combat Action Attack
  I want a character to be able to attack another one

  Background:
    Given "carrie" is a character sheet
    And "linuar" is a character sheet
    And The gaming session starts with "carrie" and "linuar"
    And close combat breaks between "carrie" and "linuar"
    And "carrie" has 1 action points left

  Scenario: A character attacks another character but misses
    When "carrie" attacks "linuar" with "bare-handed" and rolls a "failure"
    And the d100 roll is confirmed
    And "carrie" must have 0 action points left

  Scenario: A character attacks another character and hits
    When "carrie" attacks "linuar" with "bare-handed" and rolls a "success"
    And the d100 roll is confirmed
    And "carrie" must have 0 action points left