Feature: Combat Action Attack
  A character should only be able to perform a melee attack if they are engaged

  Background:
    Given "carrie" is a character sheet
    And "linuar" is a character sheet
    And The gaming session starts with "carrie" and "linuar"
    And combat breaks between "carrie" and "linuar"

  Scenario: A character is not engaged to another one so he cant melee attack him
    Then possible actions "MUST NOT" include "melee_attack carrie linuar bare-handed"

  Scenario: A character is engaged to another one so he can melee attack him
    Given "carrie" engages "linuar"
    Then possible actions "MUST" include "melee_attack carrie linuar bare-handed"