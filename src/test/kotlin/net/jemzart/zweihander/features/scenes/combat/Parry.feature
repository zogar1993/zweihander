Feature: Combat Action Attack
  I want a character to be able to parry another ones attack

  Background:
    Given "carrie" is a character sheet
    And "linuar" is a character sheet
    And The gaming session starts with "carrie" and "linuar"
    And combat breaks between "carrie" and "linuar"
    And "carrie" engages "linuar"
    And "carrie" has 1 action points left

  Scenario: A character may parry a successful attack
    Given "linuar" has 1 action points left
    When "carrie" attacks "linuar" with "bare-handed" and rolls a "success"
    And the d100 roll is confirmed
    Then possible actions "MUST" include "parry linuar bare-handed"

  Scenario: A character may not parry a successful attack if she has not enough action points
    Given "linuar" has 0 action points left
    When "carrie" attacks "linuar" with "bare-handed" and rolls a "success"
    And the d100 roll is confirmed
    Then possible actions "MUST NOT" include "parry linuar bare-handed"

  Scenario: A character parries an attack and critically succeeds keeps her action point
    Given "linuar" has 1 action points left
    And "carrie" attacks "linuar" with "bare-handed" and rolls a "success"
    And the d100 roll is confirmed
    When "linuar" parries with "bare-handed" and rolls a "critical success"
    And the d100 roll is confirmed
    Then "linuar" must have 1 action points left