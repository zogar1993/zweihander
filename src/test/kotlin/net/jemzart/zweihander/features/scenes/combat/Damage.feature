Feature: Combat Action Attack
  I want a character to be able to attack another one

  Background:
    Given "carrie" is a character sheet
    And "linuar" is a character sheet
    And The gaming session starts with "carrie" and "linuar"
    And close combat breaks between "carrie" and "linuar"
    And "carrie" has 1 action points left
    And "linuar" has 0 action points left
    And "carrie" attacks "linuar" with "bare-handed" and rolls a "success"
    And the d100 roll is confirmed

  Scenario: A character is dealt damage and lowers 1 in the condition track
    Given "linuar" must have a damage threshold value of 7
    And "linuar" must be "unharmed" in the damage condition track
    When "carrie" rolls a 4 in her damage roll
    And "linuar" has received 8 damage
    Then "linuar" must be "lightly wounded" in the damage condition track

  Scenario: A character is dealt damage and lowers 1 in the condition track
    Given "linuar" must have a damage threshold value of 7
    And "linuar" must be "unharmed" in the damage condition track
    When "carrie" rolls a 3 in her damage roll
    And "linuar" has received 7 damage
    Then "linuar" must be "unharmed" in the damage condition track