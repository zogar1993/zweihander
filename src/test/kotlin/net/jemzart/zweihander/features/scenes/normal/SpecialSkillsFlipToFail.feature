Feature: Special Skills Flip to Fail when untrained

  Background:
    Given "khorana" is a character sheet
    Given "khorana" has a "combat" base chance of 40

  Scenario Outline: when a character has 0 ranks in a special skill (<special_skill>), she flips to fail
    Given "khorana" has 0 ranks in <special_skill>
    And The gaming session starts with "khorana"
    When "khorana" performs a <special_skill> skill test
    And the d100 rolls 18
    Then action total chance of success is 40
    And the roll is a "failure"

    Examples:
      | special_skill    |
      | "martial_melee"  |
      | "martial_ranged" |

  Scenario Outline: when a character has <ranks> ranks in a special skill (<special_skill>), she does not flip to fail
    Given "khorana" has <ranks> ranks in <special_skill>
    And The gaming session starts with "khorana"
    When "khorana" performs a <special_skill> skill test
    And the d100 rolls 18
    Then action total chance of success is <total_chance>
    And the roll is a "success"

    Examples:
      | special_skill   | ranks | total_chance |
      | "martial_melee" | 1     | 50           |
      | "martial_melee" | 2     | 60           |
      | "martial_melee" | 3     | 70           |

  Scenario Outline: when a character has 0 ranks in a non-special skill (<non_special_skill>), she does not flip to fail
    Given "khorana" has 0 ranks in <non_special_skill>
    And The gaming session starts with "khorana"
    When "khorana" performs a <non_special_skill> skill test
    And the d100 rolls 18
    Then action total chance of success is 40
    And the roll is a "success"

    Examples:
      | non_special_skill |
      | "simple_melee"    |
      | "simple_ranged"   |
