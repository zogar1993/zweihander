Feature: Take Aim

  Background:
    Given "carrie" is a character sheet
    And "carrie" has a "combat" base chance of 40
    And "carrie" has 0 ranks in "simple_melee"
    And "linuar" is a character sheet
    And The gaming session starts with "carrie" and "linuar"
    And close combat breaks between "carrie" and "linuar"

  Scenario: Take Aim 1
    And "carrie" has 1 action points left
    When "carrie" takes aim with 1 action point
    Then "carrie" must have 0 action points left

  Scenario: Take Aim 2
    And "carrie" has 2 action points left
    When "carrie" takes aim with 2 action points
    Then "carrie" must have 0 action points left

  Scenario: Attack chance with Take Aim 1 is incremented by 10
    When "carrie" takes aim with 1 action point
    And "carrie" attacks "linuar" with "bare-handed"
    Then action total chance of success is 50

  Scenario: Attack chance with Take Aim 2 is incremented by 20
    When "carrie" takes aim with 2 action points
    And "carrie" attacks "linuar" with "bare-handed"
    Then action total chance of success is 60