Feature: Attribute Calculations
  Skill test Total Chance for success must be correctly calculated

  Background:
    Given "ragoz" is a character sheet

  Scenario Outline: A character makes a skill test
    Given "ragoz" has a <attribute> base chance of <base>
    And "ragoz" has <ranks> ranks in <skill>
    And The gaming session starts with "ragoz"
    When "ragoz" performs a <skill> skill test
    Then action total chance of success is <total_chance>
    Examples:
      | attribute      | base | skill           | ranks | total_chance |
      | "combat"       | 40   | "martial_melee" | 0     | 40           |
      | "brawn"        | 41   | "athletics"     | 1     | 51           |
      | "perception"   | 43   | "scrutinize"    | 2     | 63           |
      | "intelligence" | 47   | "education"     | 3     | 77           |