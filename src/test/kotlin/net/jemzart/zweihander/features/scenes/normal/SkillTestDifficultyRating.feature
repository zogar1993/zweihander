Feature: Attribute Calculations with Difficulty Ratings
  Skill test Total Chance for success must be correctly calculated based on difficulty ratings

  Background:
    Given "linuar" is a character sheet
    And "linuar" has a "perception" base chance of 40
    And "linuar" has 0 ranks in "scrutinize"
    And The gaming session starts with "linuar"

  Scenario Outline: A character makes a skill test with difficulty rating of <difficulty>
    When "linuar" performs a <difficulty> "scrutinize" skill test
    Then action total chance of success is <total_chance>
    Examples:
      | difficulty    | total_chance |
      | "trivial"     | 70           |
      | "easy"        | 60           |
      | "routine"     | 50           |
      | "standard"    | 40           |
      | "challenging" | 30           |
      | "hard"        | 20           |
      | "arduous"     | 10           |
