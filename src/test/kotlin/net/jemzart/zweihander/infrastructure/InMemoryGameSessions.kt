package net.jemzart.zweihander.infrastructure

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.GameSessions

class InMemoryGameSessions : GameSessions {
	private val sessions = mutableMapOf<String, MutableList<String>>()

	override fun search(name: String): GameSession {
		val commands = sessions[name] ?: error("session $name not found")
		val session = GameSession(name)
		commands.forEach { command -> session.execute(command) }
		return session
	}

	override fun addEntry(name: String, entry: String) {
		sessions.getValue(name).add(entry)
	}

	override fun put(session: GameSession) {
		sessions[session.name] = mutableListOf()
	}

	override fun remove(name: String) {
		sessions.remove(name)
	}

	override fun retrieveNames(): Set<String> {
		return sessions.keys
	}
}