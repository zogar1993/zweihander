package net.jemzart.zweihander.infrastructure

import net.jemzart.jsonkraken.jsonDeserialize
import net.jemzart.jsonkraken.jsonSerialize
import net.jemzart.jsonkraken.values.JsonObject
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheet
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheets
import net.jemzart.zweihander.core.infrastructure.character.CharacterSheetJsonSerialization

class InMemoryCharacterSheets : CharacterSheets {
	private val characters = mutableMapOf<String, String>()

	override fun retrieveNames(): List<String> {
		return characters.keys.toList()
	}

	override fun search(name: String): CharacterSheet {
		val text = characters.getValue(name)
		val json = text.jsonDeserialize().cast<JsonObject>()
		return CharacterSheetJsonSerialization.deserialize(json)
	}

	override fun put(character: CharacterSheet) {
		characters[character.name] = CharacterSheetJsonSerialization.toJsonValue(character).jsonSerialize()
	}

	override fun remove(name: String) {
		characters.remove(name)
	}
}