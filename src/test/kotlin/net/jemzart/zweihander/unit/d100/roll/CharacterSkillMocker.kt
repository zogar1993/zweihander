package net.jemzart.zweihander.unit.d100.roll

import io.mockk.every
import io.mockk.mockkClass
import net.jemzart.zweihander.core.domain.character.skill.CharacterSkill

fun skillWithChanceOf(amount: Int): CharacterSkill {
	val skill = mockkClass(CharacterSkill::class)
	every { skill getProperty "chance" } returns amount
	return skill
}