package net.jemzart.zweihander.unit.d100.roll

import io.mockk.every
import io.mockk.mockkClass
import net.jemzart.zweihander.core.domain.character.skill.CharacterSkill
import net.jemzart.zweihander.core.domain.d100.D100Result
import net.jemzart.zweihander.core.domain.d100.roll.D100ResultType
import net.jemzart.zweihander.core.domain.d100.roll.SkillRoll
import org.junit.Test

class FlipToFailAndSucceedSkillRoll {
	@Test
	fun `failures remain unaffected when flip to fail and succeed`() {
		val roll = SkillRoll(skill = skillWithChanceOf(50))
		roll.flipToFail()
		roll.flipToSucceed()
		for (result in listOf(52, 54, 56, 58).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.Failure)
	}

	@Test
	fun `successes remain unaffected when flip to fail and succeed`() {
		val roll = SkillRoll(skill = skillWithChanceOf(50))
		roll.flipToFail()
		roll.flipToSucceed()
		for (result in listOf(49, 47, 45, 43).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.Success)
	}

	@Test
	fun `critical failures remain unaffected when flip to fail and succeed`() {
		val roll = SkillRoll(skill = skillWithChanceOf(1))
		roll.flipToFail()
		roll.flipToSucceed()
		for (result in listOf(11, 22, 33, 44, 55, 66, 77, 88, 99, 100).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.CriticalFailure)
	}

	@Test
	fun `critical successes remain unaffected when flip to fail and succeed`() {
		val roll = SkillRoll(skill = skillWithChanceOf(99))
		roll.flipToFail()
		roll.flipToSucceed()
		for (result in listOf(1, 11, 22, 33, 44, 55, 66, 77, 88, 99).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.CriticalSuccess)
	}
}