package net.jemzart.zweihander.unit.d100.roll

import net.jemzart.zweihander.core.domain.d100.D100Result
import net.jemzart.zweihander.core.domain.d100.roll.D100ResultType
import net.jemzart.zweihander.core.domain.d100.roll.SkillRoll
import org.junit.Test

class FlipToSucceedSkillRoll {
	@Test
	fun `successes when flip to succeed is activated`() {
		val roll = SkillRoll(skillWithChanceOf(50))
		roll.flipToSucceed()
		for (result in listOf(54, 45, 90, 9).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.Success)
	}

	@Test
	fun `failures when flip to succeed is activated`() {
		val roll = SkillRoll(skillWithChanceOf(50))
		roll.flipToSucceed()
		for (result in listOf(56, 65, 98, 89).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.Failure)
	}

	@Test
	fun `critical failures remain unaffected when flip to succeed is activated`() {
		val roll = SkillRoll(skillWithChanceOf(1))
		roll.flipToSucceed()
		for (result in listOf(11, 22, 33, 44, 55, 66, 77, 88, 99, 100).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.CriticalFailure)
	}

	@Test
	fun `critical successes remain unaffected when flip to succeed is activated`() {
		val roll = SkillRoll(skillWithChanceOf(99))
		roll.flipToSucceed()
		for (result in listOf(1, 11, 22, 33, 44, 55, 66, 77, 88, 99).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.CriticalSuccess)
	}
}