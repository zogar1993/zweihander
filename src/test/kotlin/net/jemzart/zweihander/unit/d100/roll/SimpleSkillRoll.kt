package net.jemzart.zweihander.unit.d100.roll

import net.jemzart.zweihander.core.domain.d100.D100Result
import net.jemzart.zweihander.core.domain.d100.roll.D100ResultType
import net.jemzart.zweihander.core.domain.d100.roll.SkillRoll
import org.junit.Test

class SimpleSkillRoll {
	@Test
	fun `critical successes with higher base chance`() {
		val roll = SkillRoll(skillWithChanceOf(99))
		for (result in listOf(1, 11, 22, 33, 44, 55, 66, 77, 88, 99).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.CriticalSuccess)
	}

	@Test
	fun `critical failures with lower base chance`() {
		val roll = SkillRoll(skillWithChanceOf(1))
		for (result in listOf(11, 22, 33, 44, 55, 66, 77, 88, 99, 100).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.CriticalFailure)
	}

	@Test
	fun successes() {
		val roll = SkillRoll(skillWithChanceOf(50))
		for (result in listOf(2, 10, 12, 32, 49, 50).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.Success)
	}

	@Test
	fun failures() {
		val roll = SkillRoll(skillWithChanceOf(50))
		for (result in listOf(51, 52, 65, 67, 78, 98).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.Failure)
	}
}