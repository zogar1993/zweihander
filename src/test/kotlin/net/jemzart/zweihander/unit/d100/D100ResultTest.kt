package net.jemzart.zweihander.unit.d100

import net.jemzart.zweihander.core.domain.d100.D100Result
import net.jemzart.zweihander.core.domain.errors.NumberOutOfRange
import org.junit.Test

class D100ResultTest {
	@Test
	fun `1 to 100 are valid values`() {
		(1..100).forEach { number ->
			assert(D100Result.of(number).value == number)
		}
	}

	@Test(expected = NumberOutOfRange::class)
	fun `0 is out of bounds`() {
		try {
			D100Result.of(0)
		} catch (ex: NumberOutOfRange) {
			assert(ex.actual == 0)
			assert(ex.from == 1)
			assert(ex.to == 100)
			throw ex
		}
	}

	@Test(expected = NumberOutOfRange::class)
	fun `101 is out of bounds`() {
		try {
			D100Result.of(101)
		} catch (ex: NumberOutOfRange) {
			assert(ex.actual == 101)
			assert(ex.from == 1)
			assert(ex.to == 100)
			throw ex
		}
	}
}