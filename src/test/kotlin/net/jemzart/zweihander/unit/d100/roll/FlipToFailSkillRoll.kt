package net.jemzart.zweihander.unit.d100.roll

import net.jemzart.zweihander.core.domain.d100.D100Result
import net.jemzart.zweihander.core.domain.d100.roll.D100ResultType
import net.jemzart.zweihander.core.domain.d100.roll.SkillRoll
import org.junit.Test

class FlipToFailSkillRoll {

	@Test
	fun `failures when flip to fail is activated`() {
		val roll = SkillRoll(skillWithChanceOf(50))
		roll.flipToFail()
		for (result in listOf(54, 45, 37, 73).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.Failure)
	}

	@Test
	fun `successes when flip to fail is activated`() {
		val roll = SkillRoll(skillWithChanceOf(50))
		roll.flipToFail()
		for (result in listOf(34, 43, 12, 21).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.Success)
	}

	@Test
	fun `critical failures remain unaffected when flip to fail is activated`() {
		val roll = SkillRoll(skillWithChanceOf(1))
		roll.flipToFail()
		for (result in listOf(11, 22, 33, 44, 55, 66, 77, 88, 99, 100).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.CriticalFailure)
	}

	@Test
	fun `critical successes remain unaffected when flip to fail is activated`() {
		val roll = SkillRoll(skillWithChanceOf(99))
		roll.flipToFail()
		for (result in listOf(1, 11, 22, 33, 44, 55, 66, 77, 88, 99).map { D100Result.of(it) })
			assert(roll.rolled(result) == D100ResultType.CriticalSuccess)
	}
}