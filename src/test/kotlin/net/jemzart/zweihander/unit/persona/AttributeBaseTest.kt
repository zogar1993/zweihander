package net.jemzart.zweihander.unit.character

import net.jemzart.zweihander.core.domain.character.attribute.AttributeBase
import net.jemzart.zweihander.core.domain.errors.NumberOutOfRange
import org.junit.Test

class AttributeBaseTest {
	@Test
	fun `28 to 55 are valid values`() {
		(28..55).forEach { number ->
			assert(AttributeBase.of(number).value == number)
		}
	}

	@Test(expected = NumberOutOfRange::class)
	fun `27 is out of bounds`() {
		try {
			AttributeBase.of(27)
		} catch (ex: NumberOutOfRange) {
			assert(ex.actual == 27)
			assert(ex.from == 28)
			assert(ex.to == 55)
			throw ex
		}
	}

	@Test(expected = NumberOutOfRange::class)
	fun `56 is out of bounds`() {
		try {
			AttributeBase.of(56)
		} catch (ex: NumberOutOfRange) {
			assert(ex.actual == 56)
			assert(ex.from == 28)
			assert(ex.to == 55)
			throw ex
		}
	}
}