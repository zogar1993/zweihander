package net.jemzart.zweihander.unit.character

import net.jemzart.zweihander.core.domain.character.attribute.BonusAdvances
import net.jemzart.zweihander.core.domain.errors.NumberOutOfRange
import org.junit.Test

class BonusAdvancesTest {
	@Test
	fun `0 to 6 are valid values`() {
		(0..6).forEach { number ->
			assert(BonusAdvances.of(number).value == number)
		}
	}

	@Test(expected = NumberOutOfRange::class)
	fun `-1 is out of bounds`() {
		try {
			BonusAdvances.of(-1)
		} catch (ex: NumberOutOfRange) {
			assert(ex.actual == -1)
			assert(ex.from == 0)
			assert(ex.to == 6)
			throw ex
		}
	}

	@Test(expected = NumberOutOfRange::class)
	fun `7 is out of bounds`() {
		try {
			BonusAdvances.of(7)
		} catch (ex: NumberOutOfRange) {
			assert(ex.actual == 7)
			assert(ex.from == 0)
			assert(ex.to == 6)
			throw ex
		}
	}
}