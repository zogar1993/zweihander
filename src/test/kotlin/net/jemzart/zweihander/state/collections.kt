package net.jemzart.zweihander.state

import net.jemzart.zweihander.core.domain.Weapon
import net.jemzart.zweihander.core.domain.character.skill.Skill
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheets
import net.jemzart.zweihander.core.domain.session.GameSessions
import net.jemzart.zweihander.infrastructure.InMemoryCharacterSheets
import net.jemzart.zweihander.infrastructure.InMemoryGameSessions

val characterSheets: CharacterSheets = InMemoryCharacterSheets()
val weapons = mutableMapOf("bare-handed" to Weapon(Skill.SimpleMelee))//TODO cambiar por una colección de weapons despues
const val TEST_SESSION_NAME = "test_session_name"
val gameSessions: GameSessions = InMemoryGameSessions()