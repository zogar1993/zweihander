package net.jemzart.zweihander.state

import net.jemzart.zweihander.core.actions.character_sheet.*
import net.jemzart.zweihander.core.actions.scene.combat.GetCombatant
import net.jemzart.zweihander.core.actions.scene.normal.GetLastD100RollInfo
import net.jemzart.zweihander.core.actions.session.*

val getSessionLog = GetSessionLog(gameSessions)
val executeGameSessionCommand = ExecuteGameSessionCommand(gameSessions)
val skillTest = { combatant: String, skill: String ->
	executeGameSessionCommand("skill_test $combatant $skill", TEST_SESSION_NAME)
}
val skillTestWithDifficulty = { combatant: String, skill: String, difficulty: String ->
	executeGameSessionCommand("skill_test $combatant $skill $difficulty", TEST_SESSION_NAME)
}

val createCharacterSheet = CreateCharacterSheet(characterSheets)
val getCharacterSheet = GetCharacterSheet(characterSheets)
val setAncestry = SetCharacterSheetAncestry(characterSheets)
val setAttributeBase = SetCharacterSheetAttributeBase(characterSheets)
val setAttributeAdvances = SetCharacterSheetAttributeAdvances(characterSheets)
val setSkillRanks = SetCharacterSheetSkillRanks(characterSheets)
val setProfession = SetCharacterSheetProfession(characterSheets)

val createSession = CreateSession(gameSessions)
val addCharacterToSession = AddCharacterToSession(gameSessions, characterSheets)
val setFortunePoints = { amount: Int ->
	executeGameSessionCommand("set_fortune_points $amount", TEST_SESSION_NAME)
}
val retrieveCurrentOptions = RetrieveCurrentOptions(gameSessions)
val confirmRoll = { executeGameSessionCommand("confirm", TEST_SESSION_NAME) }
val meleeAttack = { attacker: String, victim: String, weapon: String ->
	executeGameSessionCommand("melee_attack $attacker $victim $weapon", TEST_SESSION_NAME)
}
val setD100Roll = { number: Int -> executeGameSessionCommand("d100 $number", TEST_SESSION_NAME) }
val getLastD100RollInfo = GetLastD100RollInfo(gameSessions)

val addCharacterToCombat = { combatant: String ->
	executeGameSessionCommand("add_combatant $combatant", TEST_SESSION_NAME)
}
val establishAllianceBetween = { combatantA: String, combatantB: String ->
	executeGameSessionCommand("alliance $combatantA $combatantB", TEST_SESSION_NAME)
}
val getCombatant = GetCombatant(gameSessions)
val setCombatantActionPoints = { combatant: String, amount: Int ->
	executeGameSessionCommand("set_combatant_ap $combatant $amount", TEST_SESSION_NAME)
}
val engageWith = { engager: String, engaged: String ->
	executeGameSessionCommand("engage $engager $engaged", TEST_SESSION_NAME)
}