package net.jemzart.zweihander.state

import net.jemzart.zweihander.core.domain.d100.roll.D100ResultType

fun resultTypeToNumber(resultType: String): Int {
	val number = when (D100ResultType.from(resultType)) {
		D100ResultType.CriticalFailure -> 100
		D100ResultType.Failure -> 98
		D100ResultType.Success -> 2
		D100ResultType.CriticalSuccess -> 1
	}
	return number
}