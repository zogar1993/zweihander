package net.jemzart.jsonkraken.helpers

internal inline fun <reified T> isNullable() = null is T