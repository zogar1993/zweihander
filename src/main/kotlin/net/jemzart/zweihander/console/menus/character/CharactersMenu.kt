package net.jemzart.zweihander.console.menus.character

import net.jemzart.zweihander.console.core.console
import net.jemzart.zweihander.console.menus.ConsoleMenu
import net.jemzart.zweihander.console.state.MenuOption
import net.jemzart.zweihander.console.state.MenuOptions
import net.jemzart.zweihander.console.state.Menus
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheet
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheets

class CharactersMenu(private val characterSheets: CharacterSheets) : ConsoleMenu {
	override val name get() = "${previous.name} -> characters"
	override val previous get() = Menus.main
	override val options = MenuOptions(
		MenuOption(name = "load",
			syntax = "load <character_name>",
			action = {
				if (it[0] in characterSheets.retrieveNames())
					Menus.character.load(it[0])
			}),
		MenuOption(name = "create",
			syntax = "create <character_name>",
			action = {
				if (it[0] in characterSheets.retrieveNames())
					throw Exception("${it[0]} is already a character")
				val character = CharacterSheet(it[0])
				characterSheets.put(character)
			}),
		MenuOption(name = "delete",
			syntax = "delete <character_name>",
			action = {
				characterSheets.remove(it[0])
			})
	)

	override fun print() {
		val names = characterSheets.retrieveNames()
		names.forEach { console.writeLine(" - $it") }
	}
}
