package net.jemzart.zweihander.console.menus.session

import net.jemzart.zweihander.console.core.console
import net.jemzart.zweihander.console.menus.ConsoleMenu
import net.jemzart.zweihander.console.state.MenuOption
import net.jemzart.zweihander.console.state.MenuOptions
import net.jemzart.zweihander.console.state.Menus
import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.GameSessions

class SessionsMenu(
	private val sessions: GameSessions) : ConsoleMenu {
	override val previous get() = Menus.main
	override val name = "sessions"
	override val options = MenuOptions(
		MenuOption(name = "load",
			syntax = "load <session_name>",
			action = {
				val name = it[0]
				if (name in sessions.retrieveNames())
					Menus.session.load(name)
			}),
		MenuOption(name = "create",
			syntax = "create <session_name>",
			action = {
				val name = it[0]
				if (name in sessions.retrieveNames())
					throw Exception("$name is already a session")
				val session = GameSession(name)
				sessions.put(session)
			}),
		MenuOption(name = "delete",
			syntax = "delete <session_name>",
			action = { sessions.remove(it[0]) })
	)

	override fun print() {
		val names = sessions.retrieveNames()
		names.forEach { console.writeLine(" - $it") }
	}
}