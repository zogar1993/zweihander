package net.jemzart.zweihander.console.menus

import net.jemzart.zweihander.console.core.console
import net.jemzart.zweihander.console.state.MenuOption
import net.jemzart.zweihander.console.state.MenuOptions
import net.jemzart.zweihander.console.state.Menus

class HelpMenu : ConsoleMenu {
	private var _option: MenuOption? = null
	private val option: MenuOption get() = _option!!
	private var _previous: ConsoleMenu? = null
	override val previous: ConsoleMenu get() = _previous!!
	override val name: String = "help"
	override val options = MenuOptions()

	fun load(option: MenuOption, previous: ConsoleMenu) {
		_option = option
		_previous = previous
		Menus.switch(this)
	}

	override fun print() {
		console.writeLine("syntax: ${option.syntax}")
	}
}