package net.jemzart.zweihander.console.menus.character

import net.jemzart.zweihander.console.menus.ConsoleMenu
import net.jemzart.zweihander.console.state.MenuOption
import net.jemzart.zweihander.console.state.MenuOptions
import net.jemzart.zweihander.console.state.Menus
import net.jemzart.zweihander.core.actions.character_sheet.*
import net.jemzart.zweihander.core.domain.DiceCup
import net.jemzart.zweihander.core.domain.character.attribute.Attribute

class CharacterMenu(
	private val getCharacterSheet: GetCharacterSheet,
	private val setCharacterSheetAncestry: SetCharacterSheetAncestry,
	private val setCharacterSheetAttributeBase: SetCharacterSheetAttributeBase,
	private val setCharacterSheetAttributeAdvances: SetCharacterSheetAttributeAdvances,
	private val setCharacterSheetSkillRanks: SetCharacterSheetSkillRanks,
	private val setProfession: SetCharacterSheetProfession) : ConsoleMenu {
	override val name get() = "${previous.name} -> $characterName"
	override val previous = Menus.characters
	private val diceCup = DiceCup()
	private var characterName = ""

//	private fun writeLine(value: Any, vararg text: Pair<String, Color>) {
//		if (value == selected)
//			console.write(" -${"--".repeat(nesting)}> ", color = Color.YELLOW)
//		else
//			console.write("  ${"  ".repeat(nesting)}  ")
//
//		text.forEach { (line, color) -> console.write(line, color = color) }
//		console.writeLine()
//	}

	fun load(character: String) {
		characterName = character
		Menus.switch(this)
	}

	override val options = MenuOptions(
		MenuOption(name = "set_attribute_base",
			syntax = "set_attribute_base <attribute||all> <28..55||roll>",
			action = {
				val attributes = if (it[0] == "all") Attribute.values() else arrayOf(Attribute.from(it[0]))
				for (attribute in attributes) {
					val amount = when {
						it[1] == "roll" -> {
							val roll = diceCup.roll3d10plus25()
//							val dice = roll.dice
//							console.showMessageBox("${attribute.value}: ${dice[0]}+${dice[1]}+${dice[2]}+25=${roll.total}")
							roll.total
						}
						it[1].toIntOrNull() != null -> it[1].toInt()
						else -> throw IllegalStateException("Expected NUMBER or 'roll', found ${it[1]}")
					}
					setCharacterSheetAttributeBase(characterName, attribute.value, amount)
				}
			}),
		MenuOption(name = "set_attribute_advances",
			syntax = "set_attribute_advances <attribute> <0..6>",
			action = { setCharacterSheetAttributeAdvances(characterName, it[0], it[1].toInt()) }),
		MenuOption(name = "set_skill_ranks",
			syntax = "set_skill_ranks <skill> <0..3>",
			action = { setCharacterSheetSkillRanks(characterName, it[0], it[1].toInt()) }),
		MenuOption(name = "set_ancestry",
			syntax = "set_ancestry <ancestry>",
			action = { setCharacterSheetAncestry(characterName, it[0]) }),
		MenuOption(name = "set_profession",
			syntax = "set_profession <profession>",
			action = { setProfession(characterName, it[0]) })
	)

	override fun print() {
		val character = getCharacterSheet(characterName)
		display(character)
	}
}
