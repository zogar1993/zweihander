package net.jemzart.zweihander.console.menus

import net.jemzart.zweihander.console.state.MenuOption
import net.jemzart.zweihander.console.state.MenuOptions
import net.jemzart.zweihander.console.state.Menus

class CombatMenu(val characters: String) : ConsoleMenu {
	override val previous get() = Menus.main
	override val name = "combat"
	override val options = MenuOptions(
		MenuOption(
			name = "load",
			syntax = "load <character_name>",
			action = {
				it[0]
			}
		)
	)

	override fun print() {

	}
}