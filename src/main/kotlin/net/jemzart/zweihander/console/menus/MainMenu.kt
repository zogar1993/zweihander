package net.jemzart.zweihander.console.menus

import net.jemzart.zweihander.console.core.console
import net.jemzart.zweihander.console.state.MenuOption
import net.jemzart.zweihander.console.state.MenuOptions
import net.jemzart.zweihander.console.state.Menus

class MainMenu : ConsoleMenu {
	override val previous = this
	override val name: String = "main"
	override val options = MenuOptions(
		MenuOption(name = "characters",
			syntax = "characters",
			action = { Menus.switch(Menus.characters) }),
		MenuOption(name = "sessions",
			syntax = "sessions",
			action = { Menus.switch(Menus.sessions) })
	)

	override fun print() {
		for (option in options)
			console.writeLine("- ${option.syntax}")
	}
}
