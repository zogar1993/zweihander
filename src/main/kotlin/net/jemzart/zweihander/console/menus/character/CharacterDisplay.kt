package net.jemzart.zweihander.console.menus.character

import net.jemzart.zweihander.console.core.console
import net.jemzart.zweihander.core.domain.character.attribute.Attribute
import net.jemzart.zweihander.core.domain.character.profession.BasicProfession
import net.jemzart.zweihander.core.domain.character.skill.Skill
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheet
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import java.awt.Color

fun display(character: CharacterSheet) {
	displayHeader(character)
	console.writeLine()
	displayAttributes(character)
	console.writeLine()
	displaySkills(character)
}

private fun displayHeader(character: CharacterSheet) {
	console.writeLine("name: ${character.name}")
	displayAncestry(character)
	displayProfession(character)
}

private fun displaySkills(character: CharacterSheet) {
	Skill.values().forEach {
		val skill = character[it]
		val name = it.value.padEnd(maxSkillNameLength)
		val chance = skill.chance.toString().padStart(2, ' ')
		val ranks = "o".repeat(skill.ranks).padEnd(3)
//		val unranks = "x".repeat(3 - skill.ranks)
		console.write("$name: ")
		console.write("$chance ", color = Color.YELLOW)
		console.write(ranks, color = Color.GREEN)
//		console.write(unranks, color = Color.RED)
		if (skill.flipsToFail)
			console.write(" FLIP TO FAIL", color = Color.RED)
		console.writeLine()
	}
}

private fun displayAttributes(character: CharacterSheet) {
	Attribute.values().forEach {
		val attribute = character[it]
		val name = it.value.padEnd(maxAttributeNameLength)
		val base = attribute.base.toString().padStart(2, '0')
		val bonus = attribute.bonus.toString().padStart(2, ' ')
		val bonusColor = when {
			attribute.misc < 0 -> Color.RED
			attribute.misc == 0 -> Color.WHITE
			attribute.misc > 0 -> Color.GREEN
			else -> throw Exception()
		}
		val advances = when (attribute.advances) {
			0 -> "   "
			1 -> ".  "
			2 -> ":  "
			3 -> ":. "
			4 -> ":: "
			5 -> "::."
			6 -> ":::"
			else -> throw NotImplementedException()
		}
		console.write("$name: ")
		console.write("$base ", color = Color.YELLOW)
		console.write("$advances ", color = Color.CYAN)
		console.write(bonus, color = bonusColor)
		console.writeLine()
	}
}

private fun displayProfession(character: CharacterSheet) {
	console.write("profession: ", color = Color.LIGHT_GRAY)
	val profession = character.profession
	console.writeLine(profession.name.toUpperCase(), color = Color.ORANGE)
	if (profession is BasicProfession) {
		console.write("archetype: ", color = Color.LIGHT_GRAY)
		console.writeLine(profession.archetype.value.toUpperCase(), color = Color.ORANGE)
	}
}

private fun displayAncestry(character: CharacterSheet) {
	console.write("ancestry: ", color = Color.LIGHT_GRAY)
	console.writeLine(character.ancestry.value.toUpperCase(), color = Color.ORANGE)
}

private val maxAttributeNameLength = Attribute.values().map { it.value }.maxBy { it.length }!!.length
private val maxSkillNameLength = Skill.values().map { it.value }.maxBy { it.length }!!.length