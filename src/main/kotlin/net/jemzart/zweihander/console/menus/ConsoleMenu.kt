package net.jemzart.zweihander.console.menus

import net.jemzart.zweihander.console.state.MenuOptions

interface ConsoleMenu {
	val previous: ConsoleMenu
	val name: String
	val options: MenuOptions
	fun print()
}