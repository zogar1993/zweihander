package net.jemzart.zweihander.console.menus.session

import net.jemzart.zweihander.console.core.console
import net.jemzart.zweihander.console.menus.ConsoleMenu
import net.jemzart.zweihander.console.state.MenuOptions
import net.jemzart.zweihander.console.state.Menus
import net.jemzart.zweihander.core.actions.session.ExecuteGameSessionCommand
import net.jemzart.zweihander.core.actions.session.RetrieveCurrentOptions

class SessionMenu(
	private val executeGameSessionCommand: ExecuteGameSessionCommand,
	private val retrieveCurrentOptions: RetrieveCurrentOptions
) : ConsoleMenu {
	override val previous get() = Menus.main
	override val name = "session"
	private var session = ""
	override val options = MenuOptions(
	)

	override fun print() {
		val options = retrieveCurrentOptions.invoke(session)
		for (option in options)
			console.writeLine(option)
	}

	fun load(session: String) {
		this.session = session
		Menus.switch(this)
	}
}