import net.jemzart.zweihander.console.core.CommandHandler
import net.jemzart.zweihander.console.core.Console
import net.jemzart.zweihander.console.core.console
import net.jemzart.zweihander.console.menus.HelpMenu
import net.jemzart.zweihander.console.menus.MainMenu
import net.jemzart.zweihander.console.menus.character.CharacterMenu
import net.jemzart.zweihander.console.menus.character.CharactersMenu
import net.jemzart.zweihander.console.menus.session.SessionMenu
import net.jemzart.zweihander.console.menus.session.SessionsMenu
import net.jemzart.zweihander.console.state.Menus
import net.jemzart.zweihander.core.actions.character_sheet.*
import net.jemzart.zweihander.core.actions.session.ExecuteGameSessionCommand
import net.jemzart.zweihander.core.actions.session.RetrieveCurrentOptions
import net.jemzart.zweihander.core.infrastructure.character.FileSystemCharacterSheets
import net.jemzart.zweihander.core.infrastructure.session.FileSystemGameSessions
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Font
import javax.swing.*


val frame = JFrame()
val scroll = JScrollPane()
val input = JTextField()
fun main() {
	UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName())

	frame.title = "Von Chap"
	frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE

	val c = JTextPane()
	c.background = Color.DARK_GRAY
	c.isEditable = false
	c.font = Font("Courier New", Font.PLAIN, 12)
	console = Console(c)

	scroll.viewport.view = c
	scroll.border = null
	scroll.isOpaque = false
	scroll.viewport.isOpaque = false
	scroll.background = Color.DARK_GRAY

	input.isEditable = true
	input.foreground = Color.WHITE
	input.caretColor = Color.WHITE
	input.isOpaque = false
	input.font = Font("Courier New", Font.PLAIN, 12)

	frame.add(input, BorderLayout.SOUTH)
	frame.add(scroll, BorderLayout.CENTER)

	frame.contentPane.background = Color.DARK_GRAY

	frame.setSize(660, 350)
	frame.setLocationRelativeTo(null)

	frame.isResizable = true
	frame.isVisible = true

	input.requestFocus()

	initialize()

	console.showMessageBox = { message: String -> JOptionPane.showMessageDialog(frame, message) }
	val program = CommandHandler()

	input.addActionListener {
		val line = input.text.trim()
		input.text = ""
		if (line.isNotEmpty())
			program.handleCommand(line)
	}
}

private fun initialize() {
	val characterSheets = FileSystemCharacterSheets()
	val gameSessions = FileSystemGameSessions()

	val executeGameSessionCommand = ExecuteGameSessionCommand(gameSessions)
	val retrieveCurrentOptions = RetrieveCurrentOptions(gameSessions)

	val getCharacterSheet = GetCharacterSheet(characterSheets)
	val setCharacterSheetAncestry = SetCharacterSheetAncestry(characterSheets)
	val setCharacterSheetAttributeBase = SetCharacterSheetAttributeBase(characterSheets)
	val setCharacterSheetAttributeAdvances = SetCharacterSheetAttributeAdvances(characterSheets)
	val setCharacterSheetSkillRanks = SetCharacterSheetSkillRanks(characterSheets)
	val setCharacterSheetProfession = SetCharacterSheetProfession(characterSheets)

	Menus.main = MainMenu()
	Menus.characters = CharactersMenu(characterSheets)
	Menus.character = CharacterMenu(
		getCharacterSheet = getCharacterSheet,
		setCharacterSheetAncestry = setCharacterSheetAncestry,
		setCharacterSheetAttributeBase = setCharacterSheetAttributeBase,
		setCharacterSheetAttributeAdvances = setCharacterSheetAttributeAdvances,
		setCharacterSheetSkillRanks = setCharacterSheetSkillRanks,
		setProfession = setCharacterSheetProfession)
	Menus.help = HelpMenu()
	Menus.sessions = SessionsMenu(gameSessions)
	Menus.session = SessionMenu(
		retrieveCurrentOptions = retrieveCurrentOptions,
		executeGameSessionCommand = executeGameSessionCommand)
	Menus.switch(Menus.main)
}