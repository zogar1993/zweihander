package net.jemzart.zweihander.console.core

import net.jemzart.zweihander.console.state.ConsoleCommand
import net.jemzart.zweihander.console.state.Menus

class CommandHandler {
	init {
		handleCommand("main")
	}

	fun handleCommand(line: String) {
		try {
			val command = ConsoleCommand(line)
			when (command.cmd) {
				"back" -> Menus.switch(Menus.active.previous)
				"main" -> Menus.switch(Menus.main)
				"exit" -> System.exit(0)
				"help" -> {
					val options = Menus.active.options
					val cmd = command.parameters[0]
					if (cmd !in options) throw Exception("$cmd is not a valid command")
					Menus.help.load(options[cmd], Menus.active)
				}
				else -> {
					val options = Menus.active.options
					if (command.cmd !in options) throw Exception("${command.cmd} is not a valid command")
					options(command)
				}
			}
		} catch (ex: Exception) {
			console.showMessageBox(ex.message ?: ex.stackTrace.joinToString("\n"))
		}

		console.clear()
		console.writeLine(Menus.active.name)
		console.writeLine()
		Menus.active.print()
	}
}