package net.jemzart.zweihander.console.core

import java.awt.Color
import javax.swing.JTextPane
import javax.swing.text.SimpleAttributeSet
import javax.swing.text.StyleConstants
import javax.swing.text.StyleContext

lateinit var console: Console

class Console(private val console: JTextPane) {
	lateinit var showMessageBox: (String) -> Unit
	private var font = "Lucida Console"
	private var size = 12

	fun clear() {
		console.document.remove(0, console.document.length)
	}

	fun write(message: String, color: Color = Color.WHITE, size: Int = this.size) {
		val styleContext = StyleContext.getDefaultStyleContext()
		var attributeSet = styleContext.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, color)
		attributeSet = styleContext.addAttribute(attributeSet, StyleConstants.FontFamily, font)
		attributeSet = styleContext.addAttribute(attributeSet, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED)
		attributeSet = styleContext.addAttribute(attributeSet, StyleConstants.FontSize, size)

		val doc = console.styledDocument
		console.caretPosition = doc.length
		doc.insertString(doc.length, message, attributeSet.copyAttributes())
	}

	fun writeLine(message: String = "", color: Color = Color.WHITE, size: Int = 12) {
		write("$message\n", color, size)
	}
}