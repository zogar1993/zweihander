package net.jemzart.zweihander.console.state

class MenuOptions(vararg options: MenuOption) : Iterable<MenuOption> {
	private val options = options.map { it.name to it }.toMap()
	override fun iterator() = options.values.iterator()
	operator fun contains(command: String) = command in options.keys
	operator fun invoke(command: ConsoleCommand) = options.getValue(command.cmd).invoke(command)
	operator fun get(command: String) = options.getValue(command)
}