package net.jemzart.zweihander.console.state

class ConsoleCommand(raw: String) {
	val cmd: String
	val parameters: List<String>

	init {
		val parts = raw.trim().split(' ').filter { it.isNotBlank() }
		cmd = parts[0].toLowerCase()
		parameters = parts.subList(1, parts.size)
	}
}