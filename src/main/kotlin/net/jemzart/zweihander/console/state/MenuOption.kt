package net.jemzart.zweihander.console.state

class MenuOption(val name: String,
                 val syntax: String,
                 private val action: (List<String>) -> Unit) {
	private val parametersAmount = syntax.split(' ').size - 1
	operator fun invoke(cmd: ConsoleCommand) {
		if (cmd.parameters.size != parametersAmount)
			throw IllegalStateException("Invalid parameters. Type 'help $name' for more info.")
		action(cmd.parameters)
	}
}