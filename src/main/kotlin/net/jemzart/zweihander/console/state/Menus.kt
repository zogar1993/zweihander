package net.jemzart.zweihander.console.state

import net.jemzart.zweihander.console.menus.ConsoleMenu
import net.jemzart.zweihander.console.menus.HelpMenu
import net.jemzart.zweihander.console.menus.MainMenu
import net.jemzart.zweihander.console.menus.character.CharacterMenu
import net.jemzart.zweihander.console.menus.character.CharactersMenu
import net.jemzart.zweihander.console.menus.session.SessionMenu
import net.jemzart.zweihander.console.menus.session.SessionsMenu

object Menus {
	lateinit var main: MainMenu
	lateinit var characters: CharactersMenu
	lateinit var sessions: SessionsMenu
	lateinit var session: SessionMenu
	lateinit var character: CharacterMenu
	lateinit var help: HelpMenu

	lateinit var active: ConsoleMenu
		private set

	fun switch(menu: ConsoleMenu) {
		active = menu
	}
}