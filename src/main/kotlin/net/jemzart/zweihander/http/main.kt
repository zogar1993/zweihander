package net.jemzart.zweihander.http

import io.ktor.application.ApplicationCall
import io.ktor.features.CORS
import io.ktor.application.install
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.response.respondText
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import net.jemzart.zweihander.http.characters.*
import java.time.Duration

fun main(){
	embeddedServer(Netty, 8080) {
		install(CORS)
		{
			method(HttpMethod.Options)
			method(HttpMethod.Get)
			method(HttpMethod.Post)
			method(HttpMethod.Put)
			method(HttpMethod.Delete)
			method(HttpMethod.Patch)
			header(HttpHeaders.AccessControlAllowHeaders)
			header(HttpHeaders.ContentType)
			header(HttpHeaders.AccessControlAllowOrigin)
			allowCredentials = true
			anyHost()
			maxAge = Duration.ofDays(1)
		}
		routing {
			skills()
			attributes()
			ancestries()

			character()
			characters()

			setAncestry()

			setSkillRanks()
			setSkillAdvance()
		}
	}.start(wait = true)
}

suspend fun ApplicationCall.respondJson(text: String) {
	respondText(text, ContentType.Application.Json)
}