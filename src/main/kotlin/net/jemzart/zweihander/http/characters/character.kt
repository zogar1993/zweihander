package net.jemzart.zweihander.http.characters

import io.ktor.application.call
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.put
import net.jemzart.jsonkraken.jsonSerialize
import net.jemzart.jsonkraken.toJsonValue
import net.jemzart.jsonkraken.values.JsonObject
import net.jemzart.zweihander.http.*

fun Routing.character() {
	get("/characters/{name}") {
		val name = call.parameters["name"]!!
		val character = getCharacterSheet(name)

		val response = JsonObject(
			"name" to character.name,
			"ancestry" to character.ancestry.value,
			"attributes" to character.attributes.map {
				it.key.value to JsonObject(
					"base" to it.value.base,
					"advances" to it.value.advances,
					"bonus" to it.value.bonus
				)
			}.toMap().toJsonValue(),
			"skills" to character.skills.map {
				it.key.value to JsonObject(
					"ranks" to it.value.ranks,
					"chance" to it.value.chance
				)
			}.toMap().toJsonValue()
		)
		call.respondJson(response.jsonSerialize())
	}
}

fun Routing.characters() {
	get("/characters/") {
		val names = characterSheets.retrieveNames()
		val response = names.toJsonValue()
		call.respondJson(response.jsonSerialize())
	}
}

fun Routing.setSkillRanks() {
	put("/characters/{name}/{skill}/ranks") {
		val name = call.parameters["name"]!!
		val skill = call.parameters["skill"]!!
		val value = call.receiveText().toInt()
		setCharacterSheetSkillRanks(name, skill, value)
		call.respondJson("{}")//TODO mmm, no se como sacarlo
	}
}

fun Routing.setSkillAdvance() {
	put("/characters/{name}/{attribute}/advance") {
		val name = call.parameters["name"]!!
		val attribute = call.parameters["attribute"]!!
		val value = call.receiveText().toInt()
		setCharacterSheetAttributeAdvances(name, attribute, value)
		call.respondJson("{}")//TODO mmm, no se como sacarlo
	}
}

fun Routing.setAncestry() {
	put("/characters/{name}/ancestry") {
		val name = call.parameters["name"]!!
		val ancestry = call.receiveText()
		setCharacterSheetAncestry(name, ancestry)
		call.respondJson("{}")//TODO mmm, no se como sacarlo
	}
}