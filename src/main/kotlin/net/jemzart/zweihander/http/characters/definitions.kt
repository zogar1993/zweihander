package net.jemzart.zweihander.http.characters

import io.ktor.application.call
import io.ktor.routing.Routing
import io.ktor.routing.get
import net.jemzart.jsonkraken.jsonSerialize
import net.jemzart.jsonkraken.toJsonValue
import net.jemzart.jsonkraken.values.JsonObject
import net.jemzart.zweihander.core.domain.character.ancestry.Ancestry
import net.jemzart.zweihander.core.domain.character.attribute.Attribute
import net.jemzart.zweihander.core.domain.character.skill.Skill
import net.jemzart.zweihander.http.respondJson

fun Routing.skills() {
	get("/skills") {
		val response = Skill.values().map { skill ->
			JsonObject(
				"name" to skill.value.split("_").joinToString(" ") { it.capitalize() },
				"code" to skill.value,
				"attribute" to skill.attribute.value,
				"special" to skill.special
			)
		}.toJsonValue()
		call.respondJson(response.jsonSerialize())
	}
}

fun Routing.attributes() {
	get("/attributes") {
		val response = Attribute.values().map { attribute ->
			JsonObject(
				"name" to attribute.value.capitalize(),
				"code" to attribute.value
			)
		}.toJsonValue()
		call.respondJson(response.jsonSerialize())
	}
}


fun Routing.ancestries() {
	get("/ancestries") {
		val response = Ancestry.values().map { ancestry ->
			JsonObject(
				"name" to ancestry.value.capitalize(),
				"code" to ancestry.value,
				"bonus" to ancestry.bonus.toJsonValue()
			)
		}.toJsonValue()
		call.respondJson(response.jsonSerialize())
	}
}