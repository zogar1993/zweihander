package net.jemzart.zweihander.core.infrastructure

import java.io.File

class FileManager(path: String) {
	private val path = System.getProperty("user.dir") + "/data/$path/"

	init {
		File(this.path).mkdirs()
	}

	fun save(name: String, data: String) = File("$path$name").writeText(data)
	fun read(name: String) = File("$path$name").readText()
	fun list(): Array<String> = File(path).list() ?: arrayOf()
	fun delete(name: String) = File("$path$name").delete()
}