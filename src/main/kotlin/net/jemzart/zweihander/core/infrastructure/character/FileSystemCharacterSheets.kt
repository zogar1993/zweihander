package net.jemzart.zweihander.core.infrastructure.character

import net.jemzart.jsonkraken.jsonDeserialize
import net.jemzart.jsonkraken.jsonSerialize
import net.jemzart.jsonkraken.values.JsonObject
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheet
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheets
import net.jemzart.zweihander.core.infrastructure.FileManager

class FileSystemCharacterSheets : CharacterSheets {
	private val repository = FileManager("characters")

	override fun retrieveNames(): List<String> {
		return repository.list().map { it.substringBefore(".json") }
	}

	override fun search(name: String): CharacterSheet {
		val text = repository.read("$name.json")
		val json = text.jsonDeserialize().cast<JsonObject>()
		return CharacterSheetJsonSerialization.deserialize(json)
	}

	override fun put(character: CharacterSheet) {
		val json = CharacterSheetJsonSerialization.toJsonValue(character).jsonSerialize(formatted = true)
		repository.save(name = "${character.name}.json", data = json)
	}

	override fun remove(name: String) {
		repository.delete("$name.json")
	}
}