package net.jemzart.zweihander.core.infrastructure.character

import net.jemzart.jsonkraken.toJsonValue
import net.jemzart.jsonkraken.values.JsonObject
import net.jemzart.zweihander.core.domain.character.ancestry.Ancestry
import net.jemzart.zweihander.core.domain.character.attribute.Attribute
import net.jemzart.zweihander.core.domain.character.profession.Profession
import net.jemzart.zweihander.core.domain.character.skill.Skill
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheet

object CharacterSheetJsonSerialization {
	fun deserialize(json: JsonObject): CharacterSheet {
		upgrade(json)
		val character = CharacterSheet(json["name"].cast())
		character.ancestry = Ancestry.from(json["ancestry"].cast())
		character.profession = Profession.from(json["profession"].cast())
		json["attributes"].cast<JsonObject>().forEach {
			val attribute = it.second.cast<JsonObject>()
			val attributeName = Attribute.from(it.first)
			character[attributeName].base = attribute["base"].cast()
			character[attributeName].advances = attribute["advances"].cast()
		}
		json["skills"].cast<JsonObject>().forEach {
			val attribute = it.second.cast<JsonObject>()
			val skill = Skill.from(it.first)
			character[skill].ranks = attribute["advances"].cast()
		}
		return character
	}

	private fun upgrade(json: JsonObject) {
		if ("ancestry" !in json.keys) json["ancestry"] = "none"
		if ("profession" !in json.keys) json["profession"] = "none"
	}

	fun toJsonValue(character: CharacterSheet): JsonObject {
		val json = JsonObject()
		val attributes =
			character.attributes.map {
				it.key.value to JsonObject("base" to it.value.base, "advances" to it.value.advances)
			}.toMap().toJsonValue()
		val skills =
			character.skills.map {
				it.key.value to JsonObject("advances" to it.value.ranks)
			}.toMap().toJsonValue()
		json["name"] = character.name
		json["ancestry"] = character.ancestry.value
		json["profession"] = character.profession.name
		json["attributes"] = attributes
		json["skills"] = skills
		return json
	}
}
