package net.jemzart.zweihander.core.infrastructure.session

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.GameSessions
import net.jemzart.zweihander.core.infrastructure.FileManager

class FileSystemGameSessions : GameSessions {
	private val repository = FileManager("sessions")
	private fun commands(name: String): List<String> {
		val text = repository.read("$name.txt")
		return if (text.isBlank()) listOf() else text.split('\n')
	}

	override fun search(name: String): GameSession {
		val session = GameSession(name)
		commands(name).forEach { command -> session.execute(command) }
		return session
	}

	override fun addEntry(name: String, entry: String) {
		val commands = repository.read("$name.txt")
		repository.save(name = "$name.txt", data = if (commands.isBlank()) entry else "$commands\n$entry")
	}

	override fun put(session: GameSession) {
		repository.save(name = "${session.name}.txt", data = "")
	}

	override fun remove(name: String) {
		repository.delete("$name.txt")
	}

	override fun retrieveNames(): Set<String> {
		return repository.list().map { it.substringBefore(".txt") }.toSet()
	}
}