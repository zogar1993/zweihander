package net.jemzart.zweihander.core.actions.session

import net.jemzart.zweihander.core.domain.session.GameSessions

class GetSessionLog(private val gameSessions: GameSessions) {
	operator fun invoke(sessionName: String): List<String> {
		val session = gameSessions.search(sessionName)
		return session.log
	}
}