package net.jemzart.zweihander.core.actions.character_sheet

import net.jemzart.zweihander.core.domain.creation.character.CharacterSheet
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheets

class GetCharacterSheet(private val characterSheets: CharacterSheets) {
	operator fun invoke(character: String): CharacterSheet {
		return characterSheets.search(character)
	}
}