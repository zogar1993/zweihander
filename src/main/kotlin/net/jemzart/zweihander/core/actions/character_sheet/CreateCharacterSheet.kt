package net.jemzart.zweihander.core.actions.character_sheet

import net.jemzart.zweihander.core.domain.creation.character.CharacterSheet
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheets

class CreateCharacterSheet(private val characterSheets: CharacterSheets) {
	operator fun invoke(characterName: String) {
		val characterSheet = CharacterSheet(characterName)
		characterSheets.put(characterSheet)
	}
}