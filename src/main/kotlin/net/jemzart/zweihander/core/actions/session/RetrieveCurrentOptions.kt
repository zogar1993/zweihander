package net.jemzart.zweihander.core.actions.session

import net.jemzart.zweihander.core.domain.session.GameSessions

class RetrieveCurrentOptions(private val gameSessions: GameSessions) {
	operator fun invoke(sessionName: String): List<String> {
		val session = gameSessions.search(sessionName)
		return session.options.map { it }
	}
}