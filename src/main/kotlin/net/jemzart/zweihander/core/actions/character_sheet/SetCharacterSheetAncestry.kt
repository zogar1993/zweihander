package net.jemzart.zweihander.core.actions.character_sheet

import net.jemzart.zweihander.core.domain.character.ancestry.Ancestry
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheets

class SetCharacterSheetAncestry(private val characterSheets: CharacterSheets) {
	operator fun invoke(characterName: String, ancestryName: String) {
		val ancestry = Ancestry.from(ancestryName)
		val character = characterSheets.search(characterName)
		character.ancestry = ancestry
		characterSheets.put(character)
	}
}