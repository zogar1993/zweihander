package net.jemzart.zweihander.core.actions.scene.combat

import net.jemzart.zweihander.core.domain.session.GameSessions
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.Combatant

class GetCombatant(private val sessions: GameSessions) {
	operator fun invoke(combatant: String, sessionName: String): Combatant {
		val session = sessions.search(sessionName)
		val combat = session.scene
		return combat.getCombatant(combatant)
	}
}