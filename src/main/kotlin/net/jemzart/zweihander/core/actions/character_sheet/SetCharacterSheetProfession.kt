package net.jemzart.zweihander.core.actions.character_sheet

import net.jemzart.zweihander.core.domain.character.profession.Profession
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheets

class SetCharacterSheetProfession(private val characterSheets: CharacterSheets) {
	operator fun invoke(characterName: String, professionName: String) {
		val profession = Profession.from(professionName)
		val character = characterSheets.search(characterName)
		character.profession = profession
		characterSheets.put(character)
	}
}