package net.jemzart.zweihander.core.actions.character_sheet

import net.jemzart.zweihander.core.domain.character.attribute.Attribute
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheets

class SetCharacterSheetAttributeBase(private val characterSheets: CharacterSheets) {
	operator fun invoke(characterName: String, attributeName: String, base: Int) {
		val attribute = Attribute.from(attributeName)
		val character = characterSheets.search(characterName)
		character[attribute].base = base
		characterSheets.put(character)
	}
}