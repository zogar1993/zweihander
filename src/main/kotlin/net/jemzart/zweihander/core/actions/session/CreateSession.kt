package net.jemzart.zweihander.core.actions.session

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.GameSessions

class CreateSession(private val gameSessions: GameSessions) {
	operator fun invoke(sessionName: String) {
		val session = GameSession(sessionName)
		gameSessions.put(session)
	}
}