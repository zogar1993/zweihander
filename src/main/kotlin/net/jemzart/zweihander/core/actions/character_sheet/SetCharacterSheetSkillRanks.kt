package net.jemzart.zweihander.core.actions.character_sheet

import net.jemzart.zweihander.core.domain.character.skill.Skill
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheets

class SetCharacterSheetSkillRanks(private val characterSheets: CharacterSheets) {
	operator fun invoke(characterName: String, skillName: String, ranks: Int) {
		val skill = Skill.from(skillName)
		val character = characterSheets.search(characterName)
		character[skill].ranks = ranks
		characterSheets.put(character)
	}
}