package net.jemzart.zweihander.core.actions.session

import net.jemzart.jsonkraken.jsonSerialize
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheets
import net.jemzart.zweihander.core.domain.session.GameSessions
import net.jemzart.zweihander.core.domain.session.actions.AddCharacterToSession
import net.jemzart.zweihander.core.infrastructure.character.CharacterSheetJsonSerialization

class AddCharacterToSession(private val gameSessions: GameSessions,
                            private val characterSheets: CharacterSheets) {
	operator fun invoke(characterName: String, sessionName: String) {
		val character = characterSheets.search(characterName)
		val session = gameSessions.search(sessionName)

		val json = CharacterSheetJsonSerialization.toJsonValue(character).jsonSerialize()
		AddCharacterToSession.execute(listOf(json), session)


		gameSessions.addEntry(sessionName, "add_character $json")
	}
}