package net.jemzart.zweihander.core.actions.session

import net.jemzart.zweihander.core.domain.session.GameSessions

class ExecuteGameSessionCommand(private val gameSessions: GameSessions) {
	operator fun invoke(command: String, sessionName: String) {
		val session = gameSessions.search(sessionName)

		session.execute(command)

		gameSessions.addEntry(sessionName, command)
	}
}