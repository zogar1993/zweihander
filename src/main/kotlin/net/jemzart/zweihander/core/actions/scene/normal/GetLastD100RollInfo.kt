package net.jemzart.zweihander.core.actions.scene.normal

import net.jemzart.zweihander.core.domain.d100.SkillAction
import net.jemzart.zweihander.core.domain.session.GameSessions

class GetLastD100RollInfo(private val gameSessions: GameSessions) {
	operator fun invoke(sessionName: String): SkillAction {
		val session = gameSessions.search(sessionName)
		return session.action
	}
}