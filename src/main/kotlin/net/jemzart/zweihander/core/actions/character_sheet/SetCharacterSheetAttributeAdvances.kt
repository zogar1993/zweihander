package net.jemzart.zweihander.core.actions.character_sheet

import net.jemzart.zweihander.core.domain.character.attribute.Attribute
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheets

class SetCharacterSheetAttributeAdvances(private val characterSheets: CharacterSheets) {
	operator fun invoke(characterName: String, attributeName: String, advances: Int) {
		val attribute = Attribute.from(attributeName)
		val character = characterSheets.search(characterName)
		character[attribute].advances = advances
		characterSheets.put(character)
	}
}