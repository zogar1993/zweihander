package net.jemzart.zweihander.core.domain.character.traits

import net.jemzart.zweihander.core.domain.Weapon
import net.jemzart.zweihander.core.domain.d100.SkillAction
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.Combatant

interface WhenAttacked : Trait {
	fun apply(victim: Combatant, attacker: Combatant, weapon: Weapon, action: SkillAction)
}