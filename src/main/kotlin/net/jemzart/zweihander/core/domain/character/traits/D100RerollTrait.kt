package net.jemzart.zweihander.core.domain.character.traits

import net.jemzart.zweihander.core.domain.character.skill.Skill
import net.jemzart.zweihander.core.domain.d100.SkillAction
import net.jemzart.zweihander.core.domain.d100.roll.D100ResultType
import net.jemzart.zweihander.core.domain.session.actions.SpecialReroll

abstract class D100RerollTrait(private val name: String,
                               vararg skills: Skill) : OnPerformingSkillTest {
	private val skills = skills.toSet()
	override fun apply(action: SkillAction) {
		if (action.skill in skills)
			action.afterRoll.whenever(D100ResultType.Failure, SpecialReroll.getCommand(name, action.character))
	}
}