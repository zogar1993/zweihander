package net.jemzart.zweihander.core.domain.character.ancestry

import net.jemzart.zweihander.core.domain.character.attribute.Attribute
import net.jemzart.zweihander.core.domain.character.attribute.Attribute.*

enum class Ancestry(val value: String, val bonus: Map<Attribute, Int>) {
	Human("human", mapOf(
		Combat to 1,
		Intelligence to 1,
		Perception to 1,
		Agility to -1,
		Fellowship to -1,
		Willpower to -1)),
	Dwarf("dwarf", mapOf(
		Combat to 1,
		Willpower to 1,
		Brawn to 1,
		Perception to -1,
		Agility to -1,
		Fellowship to -1)),
	Elf("elf", mapOf(
		Intelligence to 1,
		Perception to 1,
		Agility to 1,
		Fellowship to -1,
		Willpower to -1,
		Brawn to -1)),
	Gnome("gnome", mapOf(
		Combat to -1,
		Intelligence to 1,
		Agility to 1,
		Fellowship to -1,
		Willpower to 1,
		Brawn to -1)),
	Halfling("halfling", mapOf(
		Perception to 1,
		Agility to 1,
		Fellowship to 1,
		Combat to -1,
		Intelligence to -1,
		Brawn to -1
	)),
	Ogre("ogre", mapOf(
		Agility to -2,
		Perception to -1,
		Combat to 1,
		Brawn to 2
	)),
	None("none", emptyMap());

	companion object {
		private val all = values().map { it.value to it }.toMap()

		fun from(name: String): Ancestry {
			if (name !in all.keys) throw IllegalArgumentException("'$name' is not an ancestry")
			return all.getValue(name)
		}
	}
}