package net.jemzart.zweihander.core.domain.session.actions

import net.jemzart.zweihander.core.domain.character.GameCharacter
import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.CHARACTER
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.TRAIT

object SpecialReroll : SessionAction {
	override fun execute(params: List<String>, session: GameSession) {
		val command = "^reroll_special ${params[0]} ${params[1]}$"
		session.action.spend(command)
		session.action.reset()
	}

	fun getCommand(trait: String, character: GameCharacter): String {
		return "^reroll_special $trait ${character.name}$"
	}

	override val regex = Regex("^reroll_special$TRAIT$CHARACTER$")
}