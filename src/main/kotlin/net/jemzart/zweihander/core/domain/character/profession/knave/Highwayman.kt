package net.jemzart.zweihander.core.domain.character.profession.knave

import net.jemzart.zweihander.core.domain.character.profession.Archetype
import net.jemzart.zweihander.core.domain.character.profession.BasicProfession
import net.jemzart.zweihander.core.domain.character.skill.Skill.Intimidate
import net.jemzart.zweihander.core.domain.character.skill.Skill.Charm
import net.jemzart.zweihander.core.domain.character.traits.OnPerformingSkillTest
import net.jemzart.zweihander.core.domain.character.traits.Trait
import net.jemzart.zweihander.core.domain.d100.SkillAction

object Highwayman : BasicProfession {
	override val name = "highwayman"
	override val archetype = Archetype.Knave
	override val traits: List<Trait> = listOf(StandAndDeliver)

	object StandAndDeliver : OnPerformingSkillTest {
		override fun apply(action: SkillAction) {
			if (action.skill == Intimidate)
				action.addAlternative(action.character[Charm])
		}
	}
}