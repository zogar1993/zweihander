package net.jemzart.zweihander.core.domain.session.actions

import net.jemzart.jsonkraken.jsonDeserialize
import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.infrastructure.character.CharacterSheetJsonSerialization

object AddCharacterToSession : SessionAction {
	override fun execute(params: List<String>, session: GameSession) {
		val character = CharacterSheetJsonSerialization.deserialize(params[0].jsonDeserialize().cast())
		session.addCharacter(character)
	}

	override val regex = Regex("^add_character .*$")
}