package net.jemzart.zweihander.core.domain.character.profession

enum class Archetype(val value: String) {
	Academic("academic"),
	Commoner("commoner"),
	Knave("knave"),
	Ranger("ranger"),
	Socialite("socialite"),
	Warrior("warrior")
}