package net.jemzart.zweihander.core.domain.session.scene.combat.effects

import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.Combatant

class InitiatedViolence(val attacker: Combatant, val victim: Combatant) : Effect {
	override operator fun invoke() {
		attacker.attacked(victim)
	}
}