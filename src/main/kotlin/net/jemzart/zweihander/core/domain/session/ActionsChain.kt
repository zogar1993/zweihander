package net.jemzart.zweihander.core.domain.session

import net.jemzart.zweihander.core.domain.session.scene.combat.state.ActionLink

class ActionsChain(val session: GameSession) {
	private val actions = mutableListOf<ActionLink>()
	fun push(action: ActionLink) {
		actions.add(action)
	}

	val current get() = if (actions.isEmpty()) session.scene.idle else actions.last()

	fun end(action: ActionLink) {
		actions.remove(action)
	}
}