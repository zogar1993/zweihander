package net.jemzart.zweihander.core.domain.creation.character

interface CharacterSheets {
	fun search(name: String): CharacterSheet
	fun put(character: CharacterSheet)
	fun remove(name: String)
	fun retrieveNames(): List<String>
}