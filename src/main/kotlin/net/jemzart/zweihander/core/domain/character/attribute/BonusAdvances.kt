package net.jemzart.zweihander.core.domain.character.attribute

import net.jemzart.zweihander.core.domain.errors.NumberOutOfRange

class BonusAdvances private constructor(val value: Int) {
	companion object {
		private val results = (0..6).map { BonusAdvances(it) }
		fun of(value: Int): BonusAdvances {
			if (value !in 0..6) throw NumberOutOfRange(from = 0, to = 6, actual = value)
			return results[value]
		}
	}
}