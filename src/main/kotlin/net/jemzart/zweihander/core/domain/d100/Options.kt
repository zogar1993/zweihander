package net.jemzart.zweihander.core.domain.d100

import net.jemzart.zweihander.core.domain.d100.roll.D100ResultType

class Options {
	private val options = mutableMapOf<String, List<D100ResultType>>()
	fun options(result: D100ResultType) = options.filter { result in it.value }.map { it.key }.toSet()

	fun unless(excluded: D100ResultType, option: String) {
		val included = D100ResultType.values().filter { it != excluded }
		whenever(included, option = option)
	}

	fun always(option: String) {
		val included = D100ResultType.values().toList()
		whenever(included, option = option)
	}

	fun whenever(included: D100ResultType, option: String) {
		whenever(listOf(included), option)
	}

	fun whenever(included: List<D100ResultType>, option: String) {
		options[option] = included
	}

	fun spend(option: String) {
		if (option !in options.keys) throw Exception("option not found '$option'")
		options.remove(option)
	}
}