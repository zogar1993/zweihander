package net.jemzart.zweihander.core.domain.character.skill

import net.jemzart.zweihander.core.domain.character.attribute.Attribute
import net.jemzart.zweihander.core.domain.character.attribute.Attribute.*

enum class Skill(val value: String,
                 val attribute: Attribute,
                 val special: Boolean) {
	MartialMelee("martial_melee", attribute = Combat, special = true),
	MartialRanged("martial_ranged", attribute = Combat, special = true),
	SimpleMelee("simple_melee", attribute = Combat, special = false),
	SimpeRanged("simple_ranged", attribute = Combat, special = false),

	Athletics("athletics", attribute = Brawn, special = false),
	Drive("drive", attribute = Brawn, special = true),
	Intimidate("intimidate", attribute = Brawn, special = false),
	Thoughness("toughness", attribute = Brawn, special = false),

	Coordination("coordination", attribute = Agility, special = false),
	Pilot("pilot", attribute = Agility, special = true),
	Ride("ride", attribute = Agility, special = true),
	Skulduggery("skulduggery", attribute = Agility, special = true),
	Stealth("stealth", attribute = Agility, special = false),

	Awareness("awareness", attribute = Perception, special = false),
	Eavesdrop("eavesdrop", attribute = Perception, special = false),
	Scrutinize("scrutinize", attribute = Perception, special = false),
	Survival("survival", attribute = Perception, special = false),

	Alchemy("alchemy", attribute = Intelligence, special = true),
	Counterfeit("counterfeit", attribute = Intelligence, special = true),
	Education("education", attribute = Intelligence, special = true),
	Folklore("folklore", attribute = Intelligence, special = false),
	Gamble("gamble", attribute = Intelligence, special = false),
	Heal("heal", attribute = Intelligence, special = true),
	Navigation("navigation", attribute = Intelligence, special = true),
	Warfare("warfare", attribute = Intelligence, special = true),

	Incantation("incantation", attribute = Willpower, special = true),
	Interrogation("interrogation", attribute = Willpower, special = true),
	Resolve("resolve", attribute = Willpower, special = false),
	Tradecraft("tradecraft", attribute = Willpower, special = true),

	Bargain("bargain", attribute = Fellowship, special = false),
	Charm("charm", attribute = Fellowship, special = false),
	Disguise("disguise", attribute = Fellowship, special = true),
	Guile("guile", attribute = Fellowship, special = false),
	HandleAnimal("handle_animal", attribute = Fellowship, special = true),
	Leadership("leadership", attribute = Fellowship, special = true),
	Rumor("rumor", attribute = Fellowship, special = false);

	companion object {
		private val all = values().map { it.value to it }.toMap()

		fun from(name: String): Skill {
			if (name !in all.keys) throw IllegalArgumentException("'$name' is not a skill name")
			return all.getValue(name)
		}
	}
}