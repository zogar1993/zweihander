package net.jemzart.zweihander.core.domain.character.profession

interface BasicProfession : Profession {
	val archetype: Archetype
}