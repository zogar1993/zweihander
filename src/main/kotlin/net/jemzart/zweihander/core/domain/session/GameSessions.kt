package net.jemzart.zweihander.core.domain.session

interface GameSessions {
	fun search(name: String): GameSession
	fun addEntry(name: String, entry: String)
	fun put(session: GameSession)
	fun remove(name: String)
	fun retrieveNames(): Set<String>
}