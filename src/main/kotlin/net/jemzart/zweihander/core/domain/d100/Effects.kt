package net.jemzart.zweihander.core.domain.d100

import net.jemzart.zweihander.core.domain.d100.roll.D100ResultType
import net.jemzart.zweihander.core.domain.session.scene.combat.effects.Effect

class Effects {
	private val effects = mutableListOf<Pair<Effect, List<D100ResultType>>>()
	fun apply(result: D100ResultType) {
		val effects = effects.filter { result in it.second }.map { it.first }
		effects.forEach { it() }
	}

	fun unless(excluded: D100ResultType, effect: Effect) {
		val included = D100ResultType.values().filter { it != excluded }
		whenever(included, effect = effect)
	}

	fun always(effect: Effect) {
		val included = D100ResultType.values().toList()
		whenever(included, effect = effect)
	}

	fun whenever(included: D100ResultType, effect: Effect) {
		whenever(listOf(included), effect)
	}

	fun whenever(included: List<D100ResultType>, effect: Effect) {
		effects.add(effect to included)
	}
}