package net.jemzart.zweihander.core.domain.session.scene.combat.effects

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.scene.combat.state.ActionLink

class PushAction(val session: GameSession, val action: ActionLink) : Effect {
	override fun invoke() {
		session.push(action)
	}
}