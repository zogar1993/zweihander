package net.jemzart.zweihander.core.domain.session.actions.combat

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.SessionAction

object StartCombat : SessionAction {
	override fun execute(params: List<String>, session: GameSession) {
		session.startCombat()
	}

	override val regex = Regex("^start_combat$")
}