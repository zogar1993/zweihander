package net.jemzart.zweihander.core.domain.session.actions

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.CHARACTER
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.Combatant

object ForceEngagement : SessionAction {
	override fun execute(params: List<String>, session: GameSession) {
		val engager = session.scene.getCombatant(params[0])
		val engaged = session.scene.getCombatant(params[1])
		engager.engageWith(engaged)
		engaged.engageWith(engager)
	}

	override val regex: Regex = Regex("^engage$CHARACTER$CHARACTER$")

	fun getCommand(engager: Combatant, engaged: Combatant): String {
		return "engage ${engager.name} ${engaged.name}"
	}
}