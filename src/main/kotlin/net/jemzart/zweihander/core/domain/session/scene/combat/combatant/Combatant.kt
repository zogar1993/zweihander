package net.jemzart.zweihander.core.domain.session.scene.combat.combatant

import net.jemzart.zweihander.core.domain.Weapon
import net.jemzart.zweihander.core.domain.character.DamageConditionState
import net.jemzart.zweihander.core.domain.character.GameCharacter
import net.jemzart.zweihander.core.domain.character.attribute.Attribute
import net.jemzart.zweihander.core.domain.character.skill.Skill
import net.jemzart.zweihander.core.domain.session.scene.combat.APExpenditure

class Combatant (private val character: GameCharacter) : GameCharacter {
	override val skills get() = character.skills
	override val attributes get() = character.attributes
	override val name get() = character.name
	override val traits get() = character.traits

	override var damageCondition
		get() = character.damageCondition
		set(value) { character.damageCondition = value }

	private val _allies = mutableListOf<Combatant>()
	val allies: List<Combatant> get() = _allies
	fun alliesWith(ally: Combatant) { _allies.add(ally) }

	private val _attacked = mutableListOf<Combatant>()
	val attacked: List<Combatant> get() = _attacked
	fun attacked(combatant: Combatant) { _attacked.add(combatant) }

	private val _engaged = mutableListOf<Combatant>()
	val engaged: List<Combatant> get() = _engaged
	fun engageWith(combatant: Combatant) { _engaged.add(combatant) }

	var ap = 3
	fun spend(ap: APExpenditure) { this.ap -= ap.amount }
	fun canSpend(ap: APExpenditure): Boolean { return ap.amount <= this.ap }

	fun damageWith(weapon: Weapon) = this[Attribute.Combat].bonus

	fun receiveDamage(damage: Int) {
		if (damage > damageThreshold)
			character.damageCondition = DamageConditionState.LightlyWounded
	}

	fun getWeapon(weapon: String): Weapon {
		return Weapon(Skill.SimpleMelee)
	}
}