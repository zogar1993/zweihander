package net.jemzart.zweihander.core.domain.session.scene.combat.effects

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.scene.combat.state.ActionLink

class EndAction(val state: ActionLink, val session: GameSession) : Effect {
	override fun invoke() {
		session.end(state)
	}
}