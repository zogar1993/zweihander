package net.jemzart.zweihander.core.domain.session.actions.combat

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.SessionAction
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.CHARACTER
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.Combatant

object EstablishAlliance : SessionAction {
	override fun execute(params: List<String>, session: GameSession) {
		val combatantA = session.scene.getCombatant(params[0])
		val combatantB = session.scene.getCombatant(params[1])
		combatantA.alliesWith(combatantB)
		combatantB.alliesWith(combatantA)
	}

	override val regex = Regex("^alliance$CHARACTER$CHARACTER$")

	fun getCommand(combatantA: Combatant, combatantB: Combatant): String {
		return "alliance ${combatantA.name} ${combatantB.name}"
	}
}