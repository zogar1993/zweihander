package net.jemzart.zweihander.core.domain.character.attribute

interface CharacterAttribute {
	val base: Int
	val advances: Int
	val misc: Int
	val bonus: Int
}