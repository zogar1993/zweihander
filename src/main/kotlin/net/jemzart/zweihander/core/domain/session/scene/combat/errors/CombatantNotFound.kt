package net.jemzart.zweihander.core.domain.session.scene.combat.errors

class CombatantNotFound(val name: String) : Exception("combatant $name not found")