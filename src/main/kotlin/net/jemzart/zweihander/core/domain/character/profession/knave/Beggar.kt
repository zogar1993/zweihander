package net.jemzart.zweihander.core.domain.character.profession.knave

import net.jemzart.zweihander.core.domain.character.profession.Archetype
import net.jemzart.zweihander.core.domain.character.profession.BasicProfession
import net.jemzart.zweihander.core.domain.character.skill.Skill.Guile
import net.jemzart.zweihander.core.domain.character.skill.Skill.Intimidate
import net.jemzart.zweihander.core.domain.character.traits.D100RerollTrait
import net.jemzart.zweihander.core.domain.character.traits.Trait


object Beggar : BasicProfession {
	override val name = "beggar"
	override val archetype = Archetype.Knave
	override val traits: List<Trait> = listOf(BeggarsBowl)

	object BeggarsBowl : D100RerollTrait("beggars_bowl", Guile, Intimidate)
}