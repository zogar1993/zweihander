package net.jemzart.zweihander.core.domain.session.actions

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.NUMBER

object FortunePointsSet : SessionAction {
	override fun execute(params: List<String>, session: GameSession) {
		session.fortunePoints = params[0].toInt()
	}

	override val regex = Regex("^set_fortune_points$NUMBER$")
}