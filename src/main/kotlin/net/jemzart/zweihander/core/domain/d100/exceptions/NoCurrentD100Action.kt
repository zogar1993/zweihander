package net.jemzart.zweihander.core.domain.d100.exceptions

class NoCurrentD100Action : Exception()