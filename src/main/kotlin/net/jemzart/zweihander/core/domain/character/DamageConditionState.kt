package net.jemzart.zweihander.core.domain.character

enum class DamageConditionState(val value: String) {
	Unharmed("unharmed"),
	LightlyWounded("lightly wounded"),
	ModeratelyWounded("moderately wounded"),
	SeriouslyWounded("seriously wounded"),
	GrievouslyWounded("grievously wounded"),
	Slain("slain");

	companion object {
		private val all = values().map { it.value to it }.toMap()

		fun from(name: String): DamageConditionState {
			if (name !in all.keys) throw IllegalArgumentException("'$name' is not a damage condition name")
			return all.getValue(name)
		}
	}
}