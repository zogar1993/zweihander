package net.jemzart.zweihander.core.domain.skilltest

enum class Difficulty(val value: String, val modifier: Int) {
	Trivial("trivial", +30),
	Easy("easy", +20),
	Routine("routine", +10),
	Standard("standard", 0),
	Challenging("challenging", -10),
	Hard("hard", -20),
	Arduous("arduous", -30);

	companion object {
		private val all = values().map { it.value to it }.toMap()

		fun from(name: String): Difficulty {
			if (name !in all.keys) throw IllegalArgumentException("'$name' is not a difficulty identifier")
			return all.getValue(name)
		}
	}
}