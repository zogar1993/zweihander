package net.jemzart.zweihander.core.domain.session.scene.combat.combatant

fun Combatant.hasAttacked(combatant: Combatant) = combatant in this.attacked
fun Combatant.hasAttackedAnyOf(combatants: List<Combatant>) = combatants.any { it in this.attacked }
fun Combatant.isEngagedWith(combatant: Combatant) = combatant in this.engaged
fun Combatant.isNotEngagedWith(combatant: Combatant) = !this.isEngagedWith(combatant)