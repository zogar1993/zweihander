package net.jemzart.zweihander.core.domain.session.scene.combat.errors

class NotEngaged : Exception()