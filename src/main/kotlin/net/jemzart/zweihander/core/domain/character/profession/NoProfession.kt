package net.jemzart.zweihander.core.domain.character.profession

import net.jemzart.zweihander.core.domain.character.traits.Trait

object NoProfession : Profession {
	override val name = "none"
	override val traits: List<Trait> = listOf()
}