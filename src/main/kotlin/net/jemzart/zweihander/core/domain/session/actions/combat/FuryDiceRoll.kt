package net.jemzart.zweihander.core.domain.session.actions.combat

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.SessionAction
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.D6
import net.jemzart.zweihander.core.domain.session.scene.combat.state.OngoingAttack

object FuryDiceRoll : SessionAction {
	override fun execute(params: List<String>, session: GameSession) {
		val rolls = params.map { it.toInt() }
		val attack = session.state as OngoingAttack
		val attacker = attack.attacker
		val victim = attack.victim
		val weapon = attack.weapon

		val damage = rolls.sum() + attacker.damageWith(weapon)

		victim.receiveDamage(damage)
		session.log.add("{character:${victim.name}} received $damage damage")
	}

	override val regex = Regex("roll_[\\d]+d6$D6*")
	fun getCommand(dice: Int) = "roll_${dice}d6($D6{$dice})?"
}