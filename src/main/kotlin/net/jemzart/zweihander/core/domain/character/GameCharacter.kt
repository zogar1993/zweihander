package net.jemzart.zweihander.core.domain.character

import net.jemzart.zweihander.core.domain.character.attribute.CharacterAttribute
import net.jemzart.zweihander.core.domain.character.skill.CharacterSkill
import net.jemzart.zweihander.core.domain.character.attribute.Attribute
import net.jemzart.zweihander.core.domain.character.skill.Skill
import net.jemzart.zweihander.core.domain.character.traits.Trait

interface GameCharacter {
	val name: String
	val traits: List<Trait>
	val skills: Map<Skill, CharacterSkill>
	val attributes: Map<Attribute, CharacterAttribute>
	var damageCondition: DamageConditionState
	operator fun get(attribute: Attribute) = attributes.getValue(attribute)
	operator fun get(skill: Skill) = skills.getValue(skill)
	val damageThreshold get() = this[Attribute.Brawn].bonus + 3

	companion object {
		inline fun <reified T : Trait> traitsOf(character: GameCharacter): List<T> =
			character.traits.filterIsInstance<T>()
	}
}