package net.jemzart.zweihander.core.domain.session.actions.combat

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.SessionAction

object EndScene : SessionAction {
	override fun execute(params: List<String>, session: GameSession) {
		session.endScene()
	}

	override val regex = Regex("^end_scene$")
}