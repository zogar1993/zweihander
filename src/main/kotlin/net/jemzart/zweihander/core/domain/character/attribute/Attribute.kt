package net.jemzart.zweihander.core.domain.character.attribute

enum class Attribute(val value: String) {
	Combat("combat"),
	Brawn("brawn"),
	Agility("agility"),
	Perception("perception"),
	Intelligence("intelligence"),
	Willpower("willpower"),
	Fellowship("fellowship");

	companion object {
		private val all = values().map { it.value to it }.toMap()

		fun from(name: String): Attribute {
			if (name !in all.keys) throw IllegalArgumentException("'$name' is not an attribute name")
			return all.getValue(name)
		}
	}
}