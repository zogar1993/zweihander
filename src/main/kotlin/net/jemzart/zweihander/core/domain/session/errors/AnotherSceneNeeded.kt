package net.jemzart.zweihander.core.domain.session.errors

class AnotherSceneNeeded : Exception()