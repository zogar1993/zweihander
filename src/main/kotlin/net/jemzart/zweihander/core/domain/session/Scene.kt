package net.jemzart.zweihander.core.domain.session

import net.jemzart.zweihander.core.domain.session.errors.AnotherSceneNeeded
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.Combatant
import net.jemzart.zweihander.core.domain.session.scene.combat.state.ActionLink

interface Scene {
	val idle: ActionLink
	fun addCharacter(name: String): Unit = throw AnotherSceneNeeded()
	fun getCombatant(name: String): Combatant = throw AnotherSceneNeeded()
}