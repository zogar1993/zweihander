package net.jemzart.zweihander.core.domain.session.scene.combat.effects

interface Effect {
	operator fun invoke()
}