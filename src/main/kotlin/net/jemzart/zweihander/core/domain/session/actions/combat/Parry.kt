package net.jemzart.zweihander.core.domain.session.actions.combat

import net.jemzart.zweihander.core.domain.Weapon
import net.jemzart.zweihander.core.domain.d100.SkillAction
import net.jemzart.zweihander.core.domain.d100.roll.D100ResultType
import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.SessionAction
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.CHARACTER
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.WEAPON
import net.jemzart.zweihander.core.domain.session.actions.SkillTest
import net.jemzart.zweihander.core.domain.session.scene.combat.APExpenditure
import net.jemzart.zweihander.core.domain.session.scene.combat.actions.CombatAction
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.Combatant
import net.jemzart.zweihander.core.domain.session.scene.combat.effects.EndAction
import net.jemzart.zweihander.core.domain.session.scene.combat.effects.RemoveActionPoints
import net.jemzart.zweihander.core.domain.session.scene.combat.state.OngoingAttack
import net.jemzart.zweihander.core.domain.skilltest.Difficulty

object Parry : SessionAction {
	override val regex = Regex("^parry$CHARACTER$WEAPON\$")
	override fun execute(params: List<String>, session: GameSession) {
		val defender = session.scene.getCombatant(params[0])
		val weapon = defender.getWeapon(params[1])
		val attack = session.state as OngoingAttack

		val ap = APExpenditure.create(defender, CombatAction.Parry, 1)

		val action = SkillAction(session, defender[weapon.type])
		action.uponConfirmation.unless(D100ResultType.CriticalSuccess, effect = RemoveActionPoints(ap))
		action.uponConfirmation.whenever(D100ResultType.Success or D100ResultType.CriticalSuccess, effect = EndAction(attack, session))

		SkillTest.execute(session, defender, weapon.type, Difficulty.Standard, action)
	}

	fun getCommand(defender: Combatant, weapon: Weapon): String {
		return "^parry ${defender.name} ${weapon.name}$"
	}
}