package net.jemzart.zweihander.core.domain.session.scene.combat.state

interface ActionLink {
	fun options(): Set<String>
}