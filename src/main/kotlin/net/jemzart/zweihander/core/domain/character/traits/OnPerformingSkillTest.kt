package net.jemzart.zweihander.core.domain.character.traits

import net.jemzart.zweihander.core.domain.d100.SkillAction

interface OnPerformingSkillTest : Trait {
	fun apply(action: SkillAction)
}