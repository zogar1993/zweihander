package net.jemzart.zweihander.core.domain.d100.roll

enum class D100ResultType(val value: String, val successful: Boolean) {
	CriticalFailure("critical failure", false),
	Failure("failure", false),
	Success("success", true),
	CriticalSuccess("critical success", true);

	infix fun or(resultType: D100ResultType) = listOf(this, resultType)

	companion object {
		private val all = values().map { it.value to it }.toMap()
		fun from(name: String): D100ResultType {
			if (name !in all.keys) throw IllegalArgumentException("'$name' is not a result type")
			return all.getValue(name)
		}
	}
}