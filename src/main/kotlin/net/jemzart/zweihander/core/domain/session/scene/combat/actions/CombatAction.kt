package net.jemzart.zweihander.core.domain.session.scene.combat.actions

enum class CombatAction {
	MeleeAttack,
	Parry
}
