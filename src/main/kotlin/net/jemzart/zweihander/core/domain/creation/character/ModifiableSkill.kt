package net.jemzart.zweihander.core.domain.creation.character

import net.jemzart.zweihander.core.domain.character.GameCharacter
import net.jemzart.zweihander.core.domain.character.attribute.CharacterAttribute
import net.jemzart.zweihander.core.domain.character.skill.CharacterSkill
import net.jemzart.zweihander.core.domain.character.skill.Skill


class ModifiableSkill(
	override val skill: Skill,
	override val character: GameCharacter) : CharacterSkill {
	override var ranks = 0
	override val chance get() = character[skill.attribute].base + (ranks * 10)
}