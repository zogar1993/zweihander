package net.jemzart.zweihander.core.domain.session.actions.combat

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.SessionAction
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.CHARACTER
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.NUMBER

object CombatantActionPointsSet : SessionAction {
	override fun execute(params: List<String>, session: GameSession) {
		val combatant = session.scene.getCombatant(params[0])
		combatant.ap = params[1].toInt()
	}

	override val regex = Regex("^set_combatant_ap$CHARACTER$NUMBER$")
}