package net.jemzart.zweihander.core.domain.session.actions

import net.jemzart.zweihander.core.domain.character.GameCharacter
import net.jemzart.zweihander.core.domain.character.skill.Skill
import net.jemzart.zweihander.core.domain.character.traits.OnPerformingSkillTest
import net.jemzart.zweihander.core.domain.d100.SkillAction
import net.jemzart.zweihander.core.domain.d100.roll.D100ResultType
import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.CHARACTER
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.DIFFICULTY
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.SKILL
import net.jemzart.zweihander.core.domain.skilltest.Difficulty

object SkillTest : SessionAction {
	override fun execute(params: List<String>, session: GameSession) {
		val character = session.getCharacter(params[0])
		val skill = Skill.from(params[1])
		val difficulty = params.difficulty()

		val action = SkillAction(session, character[skill])
		execute(session, character, skill, difficulty, action)
	}

	fun execute(
		session: GameSession,
		character: GameCharacter,
		skill: Skill,
		difficulty: Difficulty,
		action: SkillAction) {

		if (character[skill].flipsToFail) action.flipToFail()
		action.difficulty = difficulty

		//TODO Esto está mal si s permite rerollear indefinidamente
		if (session.fortunePoints > 0)
			action.afterRoll.whenever(D100ResultType.Failure, SpecialReroll.getCommand("fortune_point_d100_reroll", character))

		GameCharacter.traitsOf<OnPerformingSkillTest>(character).forEach { it.apply(action) }
		session.push(action)
	}

	private fun List<String>.difficulty() =
		if (size > 2) Difficulty.from(this[2]) else Difficulty.Standard

	override val regex = Regex("^skill_test$CHARACTER$SKILL$DIFFICULTY?$")
}