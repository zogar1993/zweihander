package net.jemzart.zweihander.core.domain.session.scene.combat

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.Scene
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.Combatant
import net.jemzart.zweihander.core.domain.session.scene.combat.errors.CombatantNotFound
import net.jemzart.zweihander.core.domain.session.scene.combat.state.ActionLink
import net.jemzart.zweihander.core.domain.session.scene.combat.state.CombatIdle

class Combat(val session: GameSession) : Scene {
	override val idle: ActionLink = CombatIdle(this)
	val combatants = mutableListOf<Combatant>()

	override fun addCharacter(name: String) {
		val character = session.getCharacter(name)
		val combatant = Combatant(character)
		combatants.add(combatant)
	}

	override fun getCombatant(name: String): Combatant {
		return combatants.find { name == it.name } ?: throw CombatantNotFound(name)
	}
}