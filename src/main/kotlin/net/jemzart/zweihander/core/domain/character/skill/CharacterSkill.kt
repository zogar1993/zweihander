package net.jemzart.zweihander.core.domain.character.skill

import net.jemzart.zweihander.core.domain.character.GameCharacter

interface CharacterSkill {
	val character: GameCharacter
	val skill: Skill
	val ranks: Int
	val chance: Int
	val flipsToFail get() = skill.special && ranks == 0
}