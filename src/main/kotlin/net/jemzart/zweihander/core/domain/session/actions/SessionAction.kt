package net.jemzart.zweihander.core.domain.session.actions

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.combat.*
import net.jemzart.zweihander.core.domain.session.actions.combat.special.TakeAim
import net.jemzart.zweihander.core.domain.session.actions.special.ChooseSkill

interface SessionAction {
	fun execute(params: List<String>, session: GameSession)
	val regex: Regex

	companion object {
		private const val WORD = "( +[\\w-]+)"
		const val CHARACTER = WORD
		const val WEAPON = WORD
		const val SKILL = WORD
		const val TRAIT = WORD
		const val DIFFICULTY = "( +[\\w-]+)"
		const val NUMBER = "( +[\\d]+)"
		const val D6 = "( +[1-6])"
		fun execute(command: String, session: GameSession) {
			val action = actions.find { it.regex.matches(command) } ?: error("could not interpret '$command'")
			if (session.options.none { Regex(it).matches(command) }) error("'$command' is not a valid option now")
			action.execute(command.split(" ").drop(1), session)
		}

		private val actions = listOf(
			SkillTest,
			Parry,
			MeleeAttack,
			StartCombat,
			D100Confirmation,
			ForceEngagement,
			EstablishAlliance,
			FortunePointsSet,
			CombatantActionPointsSet,
			EndScene,
			FuryDiceRoll,
			CombatantCharacterAdd,
			RollD100,
			SpecialReroll,
			AddCharacterToSession,
			TakeAim,
			ChooseSkill)
	}
}