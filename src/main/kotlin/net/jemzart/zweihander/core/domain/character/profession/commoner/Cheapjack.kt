package net.jemzart.zweihander.core.domain.character.profession.commoner

import net.jemzart.zweihander.core.domain.character.profession.Archetype
import net.jemzart.zweihander.core.domain.character.profession.BasicProfession
import net.jemzart.zweihander.core.domain.character.skill.Skill.Bargain
import net.jemzart.zweihander.core.domain.character.skill.Skill.Counterfeit
import net.jemzart.zweihander.core.domain.character.traits.D100RerollTrait
import net.jemzart.zweihander.core.domain.character.traits.Trait

object Cheapjack : BasicProfession {
	override val name = "cheapjack"
	override val archetype = Archetype.Commoner
	override val traits: List<Trait> = listOf(GreaseTheWheels)

	object GreaseTheWheels : D100RerollTrait("grease_the_wheels", Bargain, Counterfeit)
}