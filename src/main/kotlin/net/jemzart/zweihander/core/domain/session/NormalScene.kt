package net.jemzart.zweihander.core.domain.session

import net.jemzart.zweihander.core.domain.session.scene.combat.state.Idle

object NormalScene : Scene {
	override val idle = Idle()
}