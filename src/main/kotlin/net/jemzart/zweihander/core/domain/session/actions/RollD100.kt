package net.jemzart.zweihander.core.domain.session.actions

import net.jemzart.zweihander.core.domain.d100.D100Result
import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.NUMBER

object RollD100 : SessionAction {
	override fun execute(params: List<String>, session: GameSession) {
		session.action.roll(D100Result.of(params[0].toInt()))
	}

	override val regex = Regex("^d100$NUMBER$")
}