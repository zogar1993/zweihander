package net.jemzart.zweihander.core.domain.character.profession.socialite

import net.jemzart.zweihander.core.domain.character.profession.Archetype
import net.jemzart.zweihander.core.domain.character.profession.BasicProfession
import net.jemzart.zweihander.core.domain.character.skill.Skill.Guile
import net.jemzart.zweihander.core.domain.character.skill.Skill.Leadership
import net.jemzart.zweihander.core.domain.character.traits.D100RerollTrait
import net.jemzart.zweihander.core.domain.character.traits.Trait

object Anarchist : BasicProfession {
	override val name = "anarchist"
	override val archetype = Archetype.Academic
	override val traits: List<Trait> = listOf(RabbleRousing)

	object RabbleRousing : D100RerollTrait("rabble-rousing", Guile, Leadership)
}