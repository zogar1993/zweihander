package net.jemzart.zweihander.core.domain.character.attribute

import net.jemzart.zweihander.core.domain.errors.NumberOutOfRange

class AttributeBase private constructor(val value: Int) {
	companion object {
		private val results = (28..55).map { AttributeBase(it) }
		fun of(value: Int): AttributeBase {
			if (value !in 28..55) throw NumberOutOfRange(from = 28, to = 55, actual = value)
			return results[value - 28]
		}
	}
}