package net.jemzart.zweihander.core.domain.session

import net.jemzart.zweihander.core.domain.character.GameCharacter
import net.jemzart.zweihander.core.domain.creation.character.CharacterSheet
import net.jemzart.zweihander.core.domain.d100.SkillAction
import net.jemzart.zweihander.core.domain.session.actions.SessionAction
import net.jemzart.zweihander.core.domain.session.scene.combat.Combat
import net.jemzart.zweihander.core.domain.session.scene.combat.state.ActionLink

class GameSession(val name: String) {
	private val characters = mutableListOf<GameCharacter>()
	internal fun getCharacter(name: String) = characters.findLast { name == it.name }
		?: throw IllegalArgumentException("$name is not in the session ${this.name}")

	var fortunePoints: Int = 0
	private val stack = ActionsChain(this)
	val state: ActionLink get() = stack.current
	val options get() = state.options()
	val log = mutableListOf<String>()

	fun push(action: ActionLink) {
		stack.push(action)
	}

	val action get() = stack.current as? SkillAction ?: throw IllegalStateException("no action has been taken yet")

	fun addCharacter(character: CharacterSheet) {
		characters.add(character)
	}

	var scene: Scene = NormalScene; private set
	fun startCombat() {
		scene = Combat(this)
	}

	fun endScene() {
		scene = NormalScene
	}

	fun execute(command: String) {
		SessionAction.execute(command, this)
	}

	fun end(action: ActionLink) {
		stack.end(action)
	}
}