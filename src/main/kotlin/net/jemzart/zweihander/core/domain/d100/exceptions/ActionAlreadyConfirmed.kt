package net.jemzart.zweihander.core.domain.d100.exceptions

class ActionAlreadyConfirmed : Exception()