package net.jemzart.zweihander.core.domain.d100

import net.jemzart.zweihander.core.domain.character.skill.CharacterSkill
import net.jemzart.zweihander.core.domain.character.skill.Skill
import net.jemzart.zweihander.core.domain.d100.exceptions.ActionAlreadyConfirmed
import net.jemzart.zweihander.core.domain.d100.roll.D100ResultType
import net.jemzart.zweihander.core.domain.d100.roll.SkillRoll
import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.D100Confirmation
import net.jemzart.zweihander.core.domain.session.actions.RollD100
import net.jemzart.zweihander.core.domain.session.scene.combat.state.ActionLink

open class SkillAction(val session: GameSession, skill: CharacterSkill) : SkillRoll(skill), ActionLink {
	val character = skill.character
	override fun options(): Set<String> {
		if (_result == null) return setOf(RollD100.regex.pattern)
		if (confirmable) return afterRoll.options(result)
		return afterConfirmation.options(result)
	}

	private var _result: D100ResultType? = null
	private val result get() = _result!!
	private var confirmable = false
	val uponConfirmation = Effects()
	val afterRoll = Options()
	val afterConfirmation = Options()
	val resultType get() = result.value

	init {
		afterRoll.always(D100Confirmation.regex.pattern)
	}

	fun spend(option: String) {
		afterRoll.spend(option)
		confirmable = false
	}

	fun confirm() {
		if (!confirmable) throw ActionAlreadyConfirmed()//TODO está mal pero mas adelante lo arreglo
		confirmable = false
		uponConfirmation.apply(result)
	}

	fun roll(d100Result: D100Result) {
		if (confirmable) throw NotImplementedError("not allowed to reroll")
		confirmable = true
		_result = rolled(d100Result)
	}

	fun reset() {
		confirmable = false
		_result = null
	}
//		val options = postRollEffects.getValue(result)
//		val commands = options.map { it.command }
//		when {
//			Anarchist.RabbleRousing.RABBLE_ROUSING in commands -> options.removeIf { Anarchist.RabbleRousing.RABBLE_ROUSING == it.command }
//			"fortune point" in commands -> {
//				session.fortunePoints--
//				if (session.fortunePoints-- < 1) options.removeIf { "fortune point" == it.command }
//			}
//			else -> throw Exception()
//			//TODO Esto puede generar problemas si el resultado de la accionc cambia, por ahora me chupa un huevo
//		}

}