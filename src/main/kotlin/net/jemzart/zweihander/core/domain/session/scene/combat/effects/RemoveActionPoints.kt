package net.jemzart.zweihander.core.domain.session.scene.combat.effects

import net.jemzart.zweihander.core.domain.session.scene.combat.APExpenditure

class RemoveActionPoints(val ap: APExpenditure) : Effect {
	override operator fun invoke() {
		ap.combatant.spend(ap)//TODO Esto es raro
	}
}