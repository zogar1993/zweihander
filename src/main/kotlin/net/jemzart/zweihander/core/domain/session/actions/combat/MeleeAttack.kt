package net.jemzart.zweihander.core.domain.session.actions.combat

import net.jemzart.zweihander.core.domain.Weapon
import net.jemzart.zweihander.core.domain.character.GameCharacter
import net.jemzart.zweihander.core.domain.character.traits.WhenAttacked
import net.jemzart.zweihander.core.domain.d100.modifiers.D100Modifier
import net.jemzart.zweihander.core.domain.d100.roll.D100ResultType.Success
import net.jemzart.zweihander.core.domain.d100.roll.D100ResultType.CriticalSuccess
import net.jemzart.zweihander.core.domain.d100.roll.D100ResultType.Failure
import net.jemzart.zweihander.core.domain.d100.roll.D100ResultType.CriticalFailure
import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.SessionAction
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.CHARACTER
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.WEAPON
import net.jemzart.zweihander.core.domain.session.actions.SkillTest
import net.jemzart.zweihander.core.domain.session.scene.combat.APExpenditure
import net.jemzart.zweihander.core.domain.session.scene.combat.Combat
import net.jemzart.zweihander.core.domain.session.scene.combat.actions.CombatAction
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.Combatant
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.isNotEngagedWith
import net.jemzart.zweihander.core.domain.session.scene.combat.effects.EndAction
import net.jemzart.zweihander.core.domain.session.scene.combat.effects.InitiatedViolence
import net.jemzart.zweihander.core.domain.session.scene.combat.effects.RemoveActionPoints
import net.jemzart.zweihander.core.domain.session.scene.combat.errors.NotEngaged
import net.jemzart.zweihander.core.domain.session.scene.combat.state.OngoingAttack
import net.jemzart.zweihander.core.domain.session.scene.combat.state.TakenAim
import net.jemzart.zweihander.core.domain.skilltest.Difficulty

object MeleeAttack : SessionAction {
	override val regex = Regex("^melee_attack$CHARACTER$CHARACTER$WEAPON\$")
	override fun execute(params: List<String>, session: GameSession) {
		val attacker = session.scene.getCombatant(params[0])
		val victim = session.scene.getCombatant(params[1])
		val weapon = attacker.getWeapon(params[2])

		execute(victim, attacker, weapon, session)
	}

	private fun execute(victim: Combatant, attacker: Combatant, weapon: Weapon, session: GameSession) {
		if (victim.isNotEngagedWith(attacker)) throw NotEngaged()
		val ap = APExpenditure.create(attacker, CombatAction.MeleeAttack, 1)

		val action = OngoingAttack(attacker, victim, weapon, session.scene as Combat)

		GameCharacter.traitsOf<WhenAttacked>(victim).forEach { it.apply(victim, attacker, weapon, action) }

		action.uponConfirmation.always(RemoveActionPoints(ap))
		action.uponConfirmation.always(InitiatedViolence(attacker, victim))
		action.uponConfirmation.whenever(Failure or CriticalFailure, EndAction(action, session))

		if (victim.ap > 0)
			action.afterConfirmation.whenever(Success, Parry.getCommand(victim, victim.getWeapon("bare-handed")))
		action.afterConfirmation.whenever(Success or CriticalSuccess, FuryDiceRoll.getCommand(1))

		if (session.state is TakenAim)
			action.addModifier(session.state as TakenAim)

		SkillTest.execute(session, attacker, weapon.type, Difficulty.Standard, action)
	}

	fun getCommand(attacker: Combatant, victim: Combatant, weapon: Weapon): String {
		return "melee_attack ${attacker.name} ${victim.name} ${weapon.name}"
	}
}