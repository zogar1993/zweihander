package net.jemzart.zweihander.core.domain.character.profession.academic

import net.jemzart.zweihander.core.domain.Weapon
import net.jemzart.zweihander.core.domain.character.profession.Archetype
import net.jemzart.zweihander.core.domain.character.profession.BasicProfession
import net.jemzart.zweihander.core.domain.character.traits.Trait
import net.jemzart.zweihander.core.domain.character.traits.WhenAttacked
import net.jemzart.zweihander.core.domain.d100.SkillAction
import net.jemzart.zweihander.core.domain.d100.modifiers.D100ModifierByTrait
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.Combatant
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.hasAttacked
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.hasAttackedAnyOf

object Adherent : BasicProfession {
	override val name = "adherent"
	override val archetype = Archetype.Academic
	override val traits: List<Trait> = listOf(TurnTheOtherCheek)

	object TurnTheOtherCheek : WhenAttacked {
		override fun apply(victim: Combatant, attacker: Combatant, weapon: Weapon, action: SkillAction) {
			if (victim.hasAttacked(attacker)) return
			if (victim.hasAttackedAnyOf(attacker.allies)) return
			action.addModifier(D100ModifierByTrait(-20, victim, TurnTheOtherCheek))
		}
	}
}