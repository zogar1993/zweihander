package net.jemzart.zweihander.core.domain.errors

import net.jemzart.zweihander.core.domain.session.scene.combat.APExpenditure
import net.jemzart.zweihander.core.domain.session.scene.combat.actions.CombatAction
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.Combatant

class NotEnoughActionPoints(APExpenditure: APExpenditure) : Exception() {
	val performing: CombatAction = APExpenditure.action
	val character: Combatant = APExpenditure.combatant
	val needed: Int = APExpenditure.amount
}