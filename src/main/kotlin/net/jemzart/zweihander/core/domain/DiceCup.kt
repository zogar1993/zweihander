package net.jemzart.zweihander.core.domain

import kotlin.random.Random

class DiceCup {
	class AttributeRoll(val dice: List<Int>) {
		val total = dice.sum() + 25
	}

	fun roll3d10plus25(): AttributeRoll {
		return AttributeRoll(listOf(Random.nextInt(1, 11), Random.nextInt(1, 11), Random.nextInt(1, 11)))
	}
}