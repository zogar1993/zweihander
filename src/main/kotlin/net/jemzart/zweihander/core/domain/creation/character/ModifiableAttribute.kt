package net.jemzart.zweihander.core.domain.creation.character

import net.jemzart.zweihander.core.domain.character.attribute.CharacterAttribute
import net.jemzart.zweihander.core.domain.character.attribute.AttributeBase
import net.jemzart.zweihander.core.domain.character.attribute.BonusAdvances

class ModifiableAttribute : CharacterAttribute {
	private var _base = AttributeBase.of(42)
	override var base: Int
		get() = _base.value
		set(value) {
			_base = AttributeBase.of(value)
		}

	private var _advances = BonusAdvances.of(0)
	override var advances: Int
		get() = _advances.value
		set(value) {
			_advances = BonusAdvances.of(value)
		}

	override var misc: Int = 0
	override val bonus get() = (base / 10) + advances + misc
}