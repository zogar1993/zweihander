package net.jemzart.zweihander.core.domain.errors

class NumberOutOfRange(val from: Int,
                       val to: Int,
                       val actual: Int) : Exception()