package net.jemzart.zweihander.core.domain.session.scene.combat

import net.jemzart.zweihander.core.domain.errors.NotEnoughActionPoints
import net.jemzart.zweihander.core.domain.session.scene.combat.actions.CombatAction
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.Combatant

class APExpenditure private constructor(val combatant: Combatant,
                                        val action: CombatAction,
                                        val amount: Int) {
	companion object {
		fun create(combatant: Combatant, action: CombatAction, amount: Int): APExpenditure {
			val ap = APExpenditure(amount = amount, combatant = combatant, action = action)
			if (!combatant.canSpend(ap)) throw NotEnoughActionPoints(ap)
			return ap
		}
	}
}