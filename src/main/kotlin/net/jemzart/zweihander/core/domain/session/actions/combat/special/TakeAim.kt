package net.jemzart.zweihander.core.domain.session.actions.combat.special

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.SessionAction
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.CHARACTER
import net.jemzart.zweihander.core.domain.session.scene.combat.APExpenditure
import net.jemzart.zweihander.core.domain.session.scene.combat.Combat
import net.jemzart.zweihander.core.domain.session.scene.combat.actions.CombatAction
import net.jemzart.zweihander.core.domain.session.scene.combat.state.TakenAim

object TakeAim : SessionAction {
	override val regex = Regex("^take_aim$CHARACTER (1|2)\$")
	override fun execute(params: List<String>, session: GameSession) {
		val character = session.scene.getCombatant(params[0])
		val actionPoints = params[1].toInt()

		val ap = APExpenditure.create(character, CombatAction.MeleeAttack, actionPoints)
		val takenAim = TakenAim(character, actionPoints * 10, session.scene as Combat)

		character.spend(ap)
		session.push(takenAim)
	}
}