package net.jemzart.zweihander.core.domain.d100.modifiers

interface D100Modifier {
	val modifier: Int
}