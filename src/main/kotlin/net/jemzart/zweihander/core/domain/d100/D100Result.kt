package net.jemzart.zweihander.core.domain.d100

import net.jemzart.zweihander.core.domain.errors.NumberOutOfRange

class D100Result private constructor(val value: Int) {
	companion object {
		private val results = (1..100).map { D100Result(it) }
		fun of(value: Int): D100Result {
			if (value !in 1..100) throw NumberOutOfRange(from = 1, to = 100, actual = value)
			return results[value - 1]
		}
	}
}