package net.jemzart.zweihander.core.domain.d100.modifiers

import net.jemzart.zweihander.core.domain.character.traits.Trait
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.Combatant

class D100ModifierByTrait(override val modifier: Int,
                          val combatant: Combatant,
                          val trait: Trait) : D100Modifier