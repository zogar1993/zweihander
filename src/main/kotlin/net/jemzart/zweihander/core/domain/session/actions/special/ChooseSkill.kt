package net.jemzart.zweihander.core.domain.session.actions.special

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.SessionAction
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.SKILL

object ChooseSkill : SessionAction {
	override fun execute(params: List<String>, session: GameSession) {
		//podria reemplazarse con el confirm
	}

	override val regex = Regex("^choose_skill$SKILL$")
}