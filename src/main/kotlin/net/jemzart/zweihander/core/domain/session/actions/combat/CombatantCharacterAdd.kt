package net.jemzart.zweihander.core.domain.session.actions.combat

import net.jemzart.zweihander.core.domain.session.GameSession
import net.jemzart.zweihander.core.domain.session.actions.SessionAction
import net.jemzart.zweihander.core.domain.session.actions.SessionAction.Companion.CHARACTER

object CombatantCharacterAdd : SessionAction {
	override fun execute(params: List<String>, session: GameSession) {
		session.scene.addCharacter(params[0])
	}

	override val regex = Regex("^add_combatant$CHARACTER$")
}