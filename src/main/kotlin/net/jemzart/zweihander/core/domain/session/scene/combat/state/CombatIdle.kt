package net.jemzart.zweihander.core.domain.session.scene.combat.state

import net.jemzart.zweihander.core.domain.session.actions.AddCharacterToSession
import net.jemzart.zweihander.core.domain.session.actions.ForceEngagement
import net.jemzart.zweihander.core.domain.session.actions.FortunePointsSet
import net.jemzart.zweihander.core.domain.session.actions.combat.*
import net.jemzart.zweihander.core.domain.session.actions.combat.special.TakeAim
import net.jemzart.zweihander.core.domain.session.scene.combat.Combat
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.isEngagedWith

class CombatIdle(private val combat: Combat) : ActionLink {
	private val combatants get() = combat.combatants
	val actions = setOf(
		AddCharacterToSession,
		EndScene,
		CombatantCharacterAdd,
		ForceEngagement,
		CombatantActionPointsSet,
		FortunePointsSet,
		CombatantActionPointsSet,
		TakeAim).map { it.regex.pattern }.toSet()

	override fun options(): Set<String> {
		val options = mutableListOf<String>()

		combatants.forEach { character ->
			combatants.forEach { other ->
				if (character != other) {
					if (character.isEngagedWith(other))
						options.add(MeleeAttack.getCommand(character, other, character.getWeapon("bare-handed")))
					options.add(ForceEngagement.getCommand(character, other))
					options.add(EstablishAlliance.getCommand(character, other))
				}
			}
		}

		return actions + options
	}
}