package net.jemzart.zweihander.core.domain.d100.roll

enum class FlipMode {
	FlipToFail,
	Normal,
	FlipToSucceed;

	fun degrade(): FlipMode {
		return when (this) {
			FlipToFail -> throw Exception()
			Normal -> FlipToFail
			FlipToSucceed -> Normal
		}
	}

	fun upgrade(): FlipMode {
		return when (this) {
			FlipToFail -> Normal
			Normal -> FlipToSucceed
			FlipToSucceed -> throw Exception()
		}
	}
}