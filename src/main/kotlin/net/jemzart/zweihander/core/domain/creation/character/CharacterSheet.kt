package net.jemzart.zweihander.core.domain.creation.character

import net.jemzart.zweihander.core.domain.character.GameCharacter
import net.jemzart.zweihander.core.domain.character.ancestry.Ancestry
import net.jemzart.zweihander.core.domain.character.attribute.Attribute
import net.jemzart.zweihander.core.domain.character.profession.NoProfession
import net.jemzart.zweihander.core.domain.character.profession.Profession
import net.jemzart.zweihander.core.domain.character.skill.Skill
import net.jemzart.zweihander.core.domain.character.traits.Trait
import net.jemzart.zweihander.core.domain.character.DamageConditionState
import net.jemzart.zweihander.core.domain.character.attribute.CharacterAttribute
import net.jemzart.zweihander.core.domain.character.skill.CharacterSkill
import java.util.*

class CharacterSheet(override val name: String) : GameCharacter {
	var profession: Profession = NoProfession
		set(value) {
			_traits.removeAll(field.traits)//TODO no contempla multiples veces el mismo trait en distintas profesiones
			_traits.addAll(value.traits)
			field = value
		}
	var ancestry: Ancestry = Ancestry.None
		set(value) {
			ancestry.bonus.forEach { attributes.getValue(it.key).misc -= it.value }
			value.bonus.forEach { attributes.getValue(it.key).misc += it.value }
			field = value
		}

	override var damageCondition = DamageConditionState.Unharmed
	private val _traits = mutableListOf<Trait>()
	override val traits: List<Trait> get() = _traits
	inline fun <reified T : Trait> traits(): List<T> = traits.filterIsInstance<T>()

	override val attributes = EnumMap(Attribute.values().map { it to ModifiableAttribute() }.toMap())
	override val skills = EnumMap(Skill.values().map {
		it to ModifiableSkill(it, this)
	}.toMap())

	override operator fun get(attribute: Attribute): ModifiableAttribute = attributes.getValue(attribute)
	override operator fun get(skill: Skill): ModifiableSkill = skills.getValue(skill)
}