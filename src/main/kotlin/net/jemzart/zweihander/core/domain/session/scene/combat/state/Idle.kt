package net.jemzart.zweihander.core.domain.session.scene.combat.state

import net.jemzart.zweihander.core.domain.session.actions.AddCharacterToSession
import net.jemzart.zweihander.core.domain.session.actions.FortunePointsSet
import net.jemzart.zweihander.core.domain.session.actions.RollD100
import net.jemzart.zweihander.core.domain.session.actions.SkillTest
import net.jemzart.zweihander.core.domain.session.actions.combat.StartCombat

class Idle : ActionLink {
	val actions = setOf(
		AddCharacterToSession.regex.pattern,
		StartCombat.regex.pattern,
		SkillTest.regex.pattern,
		RollD100.regex.pattern,
		FortunePointsSet.regex.pattern)

	override fun options(): Set<String> {
		return actions.toSet()
	}
}