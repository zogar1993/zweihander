package net.jemzart.zweihander.core.domain.d100.roll

import net.jemzart.zweihander.core.domain.character.skill.CharacterSkill
import net.jemzart.zweihander.core.domain.d100.D100Result
import net.jemzart.zweihander.core.domain.d100.modifiers.D100Modifier
import net.jemzart.zweihander.core.domain.skilltest.Difficulty

open class SkillRoll(skill: CharacterSkill) {
	val alternatives = mutableListOf(skill)
	val skill = skill.skill
	val baseChance get() = alternatives.maxBy { it.chance }!!.chance
	private val modifiers = mutableListOf<D100Modifier>()
	val totalChance get() = baseChance + modifiers.map { it.modifier }.sum() + difficulty.modifier
	private var flipMode = FlipMode.Normal
	var difficulty: Difficulty = Difficulty.Standard

	fun flipToFail() {
		flipMode = flipMode.degrade()
	}

	fun flipToSucceed() {
		flipMode = flipMode.upgrade()
	}

	fun addModifier(modifier: D100Modifier) {
		modifiers.add(modifier)
	}

	fun addAlternative(skill: CharacterSkill) {
		alternatives.add(skill)
	}

	private fun flip(a: Int) = (a % 10) * 10 + a / 10
	private fun Int.isMatch() = this % 11 == 0
	fun rolled(d100Result: D100Result): D100ResultType {
		val roll = d100Result.value
		return when {
			roll == 100 -> D100ResultType.CriticalFailure
			roll == 1 -> D100ResultType.CriticalSuccess
			roll.isMatch() -> if (roll <= totalChance) D100ResultType.CriticalSuccess else D100ResultType.CriticalFailure
			else -> {
				if (roll <= totalChance)
					if (flipMode == FlipMode.FlipToFail)
						if (flip(roll) > totalChance) D100ResultType.Failure
						else D100ResultType.Success
					else D100ResultType.Success
				else
					if (flipMode == FlipMode.FlipToSucceed)
						if (flip(roll) <= totalChance) D100ResultType.Success
						else D100ResultType.Failure
					else D100ResultType.Failure
			}
		}
	}
}