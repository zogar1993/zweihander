package net.jemzart.zweihander.core.domain.character.profession

import net.jemzart.zweihander.core.domain.character.profession.academic.Adherent
import net.jemzart.zweihander.core.domain.character.profession.commoner.Cheapjack
import net.jemzart.zweihander.core.domain.character.profession.knave.Beggar
import net.jemzart.zweihander.core.domain.character.profession.knave.Highwayman
import net.jemzart.zweihander.core.domain.character.profession.socialite.Anarchist
import net.jemzart.zweihander.core.domain.character.traits.Trait

interface Profession {
	val name: String
	val traits: List<Trait>

	companion object {
		private val academics = setOf(Adherent)
		private val commoners = setOf(Cheapjack)
		private val knave = setOf(Beggar, Highwayman)
		private val socialites = setOf(Anarchist)
		private val professions = (
			academics + commoners + knave +
				socialites + NoProfession).map { it.name to it }.toMap()

		fun from(name: String): Profession {
			if (name !in professions.keys) throw IllegalArgumentException("'$name' is not a profession")
			return professions.getValue(name)
		}
	}
}