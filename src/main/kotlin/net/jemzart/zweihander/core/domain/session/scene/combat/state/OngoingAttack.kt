package net.jemzart.zweihander.core.domain.session.scene.combat.state

import net.jemzart.zweihander.core.domain.Weapon
import net.jemzart.zweihander.core.domain.d100.SkillAction
import net.jemzart.zweihander.core.domain.session.scene.combat.Combat
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.Combatant

class OngoingAttack(
	val attacker: Combatant,
	val victim: Combatant,
	val weapon: Weapon,
	val combat: Combat) : SkillAction(combat.session, attacker[weapon.type])