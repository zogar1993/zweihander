package net.jemzart.zweihander.core.domain.session.scene.combat.state

import net.jemzart.zweihander.core.domain.d100.modifiers.D100Modifier
import net.jemzart.zweihander.core.domain.session.actions.combat.special.TakeAim
import net.jemzart.zweihander.core.domain.session.scene.combat.Combat
import net.jemzart.zweihander.core.domain.session.scene.combat.combatant.Combatant

class TakenAim(
	val combatant: Combatant,
	override val modifier: Int,
	val combat: Combat) : ActionLink, D100Modifier {
	val options = combat.session.options.filter { it != TakeAim.regex.pattern }.toSet()
	override fun options() = options
}